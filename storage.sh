#!/bin/bash
set -u #exit and display error message if a variable is empty 


project_dir=$(dirname $(dirname $(realpath -s $0)))

source "$project_dir"/workflow_commands/log_manager.sh # load the log manager script

help_function() {
   echo ""
   echo "Usage: ./storage.sh DirectoryName "
   echo ""
   exit 1 # Exit script after printing help
}

call_function() {
	# call the cript passed in parameters, save it in logs
	# end the program if the called script has returned an error
	
	function_command=$@
	log_script_call "$container_path" "$function_command"
	$function_command # execute the command
	if [ ! $? = 0 ] # test if command failed
	then
		echo "error in $(basename $2)"
		echo "canceling storage"
		exit 1
	fi
}


#-----------------------------------------------#
######### ====== read parameters ====== #########
#-----------------------------------------------#

while [[ "$#" -gt 0 ]]; do
	case "$1" in
		-h | --help ) help_function ; exit 1 ;;
		-* ) echo "unknown parameter $1" ; exit 1 ;;
		* ) if test -z "${dir_path:-}" ; then
				dir_path="${1}" ; shift ;
			else
				echo "unknown parameter $1" ; exit 1 ;
			fi ;;
	esac
done


if test -z "${dir_path:-}"
then
	echo "directory path missing"
	help_function
	exit 1
fi


if [ ! -d "$dir_path" ] 
then
    echo "error : directory $dir_path not found" 
    exit 1
fi 


#---------------------------------------#
######### ====== storage ====== #########
#---------------------------------------#

#----Init Container----#


container_path="$(basename -- $dir_path)"_container_2

rm -r "$container_path"
mkdir -p "$container_path"


#----Pre processing----#

file_pre_processing_script="$project_dir"/source_encoding/pre_processing.py

call_function python3 "$file_pre_processing_script" -i "$dir_path" -o "$container_path"


#----eBlock design----#

eBlock_design_script="$project_dir"/synthesis_modules/eBlocks_design.py
eBlock_design_dir_path="$container_path"/3_eBlocks_design

mkdir -p "$eBlock_design_dir_path"

call_function python3 "$eBlock_design_script" -i "$container_path"/2_payload_fragments -o "$eBlock_design_dir_path"


#----Assembly simulation----#

assembly_simulation_script="$project_dir"/synthesis_modules/eBlock_assembly_simulation.py
eBlock_assembly_dir_path="$container_path"/4_assembly_simulation

mkdir -p "$eBlock_assembly_dir_path"

call_function python3 "$assembly_simulation_script" -i "$eBlock_design_dir_path" -o "$eBlock_assembly_dir_path" -n 1000


#----Sequencing simulation----#

simu_seq_bc_script="$project_dir"/sequencing_modules/sequencing_basecalling_simulator/sequencing_basecalling_simulator.jl
sequenced_reads_dir_path="$container_path"/5_sequenced_reads

mkdir -p "$sequenced_reads_dir_path"

#TODOcall_function julia "$simu_seq_bc_script" "$eBlock_assembly_dir_path" "$sequenced_reads_dir_path"
cp "$eBlock_assembly_dir_path"/* "$sequenced_reads_dir_path"

#----Consensus----#

consensus_script="$project_dir"/sequencing_modules/reads_consensus_class.py
consensus_dir_path="$container_path"/6_consensus

mkdir -p "$consensus_dir_path"

for sequencing_name in $(find "$sequenced_reads_dir_path" -maxdepth 1 -mindepth 1 -type f -exec basename {} ';') ; do

    sequencing_file_path="$sequenced_reads_dir_path"/$sequencing_name

	# consensus name is {start_p}_{stop_p}.fastq
    # extract start and stop primers from file name
    sep_point=(${sequencing_name//./ }) # list from split by . to remove the .fastq
    primers_list=(${sep_point[0]//_/ }) # list of start and stop primer by spliting on _

    output_consensus_path="$consensus_dir_path"/${sequencing_name//.fastq/.fasta} # replace the .fastq to .fasta for the result file name   
   
	call_function python3 "$consensus_script" -i $sequencing_file_path -o $output_consensus_path --start ${primers_list[0]} --stop ${primers_list[1]}
done


#----Payload extraction----#

payload_extraction_script="$project_dir"/sequencing_modules/extract_payload_from_assembly.py
extracted_payload_dir_path="$container_path"/7_extracted_payload

mkdir -p "$extracted_payload_dir_path"

call_function python3 "$payload_extraction_script" -i "$consensus_dir_path" -o "$extracted_payload_dir_path"


#----Post processing----#

file_post_processing_script="$project_dir"/source_encoding/post_processing.py

call_function python3 "$file_post_processing_script" -i "$extracted_payload_dir_path" -o "$container_path"

# quick diff test in the directories

diff -arqyl "$dir_path" "$container_path"/10_original_directory/"$(basename -- $dir_path)"

exit 0




