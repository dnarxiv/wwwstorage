#!/usr/bin/python3

import os
import sys
import inspect
import time
import argparse
import subprocess
import graphviz

currentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr

"""
used to get the consensus sequence from a .fastq reads file of a semi-ordered assembly and a couple of primers

there is a set of X possible fragments A, randomly concatened to a set of X possible fragments B, etc until J
each concatenation is made by a specific known overhang, ovA->B, ovB->C, ..., ovI->J
this algo aims to retrieve a list of consensus fragments A, B, ... J
"""

# global variables
oligo_names = "ABCDEFGHIJ"
oligo_sizes = {0: 158, 9:158} | {k: 167 for k in range(1,9)} # expected sizes of oligos, 1st and 9th are shorter
assembly_seq_size = 1728 # expected length of full assembly
overhangs_list = list(dfr.read_fasta("overhangs_9.fasta").values()) # list of overhangs used in the assembly
overhangs_end_position = {182 + num*171: num for num in range(9)} # position in consensus where the 4 last bases must be the numeroted known overhang

kmer_occurrences_dict = {} # dict of key:kmer, value=number of occurrences in reads
short_kmer_occurrences_dict = {} # dict of key:kmer, value=number of occurrences in reads, occurrences of shorter length


check_points_kmers = {} # primers at the start of an oligo, saved to avoid starting consensus from them more than one time -> if not breaks by exponential complexity


def count_kmers(input_path: str, kmer_size: int, min_occ: int) -> dict:
    """
    use the dsk tool to count the occurrences of each kmers in the fastq reads
    returns the dict of kmer occurrences filtered with a threshold of minimum occurrence
    """
    #start = time.time()
    if not os.path.isfile(input_path) or os.stat(input_path).st_size == 0:
        print(input_path,"is an empty file")
        return {}

    dsk_dir_path = os.path.dirname(currentdir)+"/sequencing_modules/dsk" # path to dsk algorythm directory

    dsk_script_path = dsk_dir_path+"/build/bin/dsk" # path of dsk builded script
    dsk_tempfile_path = dsk_dir_path+"/tmp/kmer_count" # dir for temporay files

    kmer_count_command = dsk_script_path+' -file '+input_path+' -out '+dsk_tempfile_path+' -kmer-size '+ str(kmer_size)+' -abundance-min '+str(min_occ)+' -verbose 0'
    subprocess.call('/bin/bash -c "$DSK"', shell=True, env={'DSK': kmer_count_command})

    # convert the result into a txt file -> lines of "kmer occurrences"
    dsk_ascii_script_path = dsk_dir_path+"/build/bin/dsk2ascii" # -file dsk_tempfile_path -out test.txt
    dsk_tempfile_txt_path = dsk_dir_path+"/tmp/kmer_count.txt"

    count_convert_command = dsk_ascii_script_path+' -file '+dsk_tempfile_path+'.h5 -out '+dsk_tempfile_txt_path+' -verbose 0'
    subprocess.call('/bin/bash -c "$CONVERT"', shell=True, env={'CONVERT': count_convert_command})

    #count_time = time.time() - start
    #print("\ncounting",count_time,"seconds")
    #start = time.time()

    kmer_occurrences_dict = {}
    # parse the file of kmer occurrences and save the lines in a dictionary
    with open(dsk_tempfile_txt_path) as count_file:
        for line in count_file:
            kmer_sequence, occurrences_str = line.split(" ")
            occurrences = int(occurrences_str)

            kmer_occurrences_dict[kmer_sequence] = occurrences
            # also save the occurrences for the reverse complement of the kmer since dsk use canonical representation of kmers
            kmer_occurrences_dict[dfr.reverse_complement(kmer_sequence)] = occurrences


    #print("\nloading",time.time() - start,"seconds")

    return kmer_occurrences_dict


def pre_filter(sequence: str) -> bool:
    """
    filter the strange sequences, that are likely sequencing errors
    True if bad sequence, False otherwise
    """

    bases = ["A", "C", "G", "T"]

    # size h homopolymeres
    h = 10
    if any(base*h in sequence for base in bases):
        #print("removed homopolymere : ",sequence)
        return True
    
    # size h2 pairs of bases repetitions that clearly are bugs
    h2 = 10
    if any((base1+base2)*h2 in sequence for base1 in bases for base2 in bases):
        #print("removed 2-repetition : ",sequence)
        return True

    if any((base1+base2+base3)*h2 in sequence for base1 in bases for base2 in bases for base3 in bases):
        #print("removed 3-repetition : ",sequence)
        return True

    if any((base1+base2+base3+base4)*h2 in sequence for base1 in bases for base2 in bases for base3 in bases for base4 in bases):
        #print("removed 3-repetition : ",sequence)
        return True

    #if any((base1+base2+base3+base4+base5)*h2 in sequence for base1 in bases for base2 in bases for base3 in bases for base4 in bases for base5 in bases):
        #print("removed 3-repetition : ",sequence)
    #    return True

    return False


def compute_all_consensus(kmer_size, short_kmer_size, start_consensus, end_consensus, reversed=False):

    """
    extract a dict of all identified oligo for each oligo name (A to J)
    starts from the beginning of the assembled mol (oligo A) and stop to the end (oligo J)
    can be run in reverse mode, so it starts from J and go back to A

    uses a list of ongoing paths to explore, get the 4 overlapping next kmers of the current path
    and continue with the associated extended paths if these kmers exists in the reads
    """


    oligos_dict = { oligo_name : [] for oligo_name in oligo_names} # store the A-J identified oligos

    # init the first paths to explore
    if kmer_size <= len(start_consensus):
        path_stack = [start_consensus] # start the exploration from the start primer
    else:
        # start from all kmer starting with the start primer
        path_stack = list(kmer for kmer in kmer_occurrences_dict if kmer.startswith(start_consensus))

    # while some paths are left to explore
    while path_stack:
        # pop a path from the list
        total_path = path_stack.pop()

        # when path length reached the maximum assembly size, stop this recursion
        if len(total_path) == assembly_seq_size:
            # if successfull end, save the oligo as the last one
            if total_path.endswith(end_consensus):
                #print("reached end")
                # extract the oligo from the path minus the 20 primer bases
                identified_oligo = total_path[-(oligo_sizes[9]+20):-20]

                # save the identified oligo
                if not reversed:
                    oligos_dict["J"].append(identified_oligo)
                else: # in reversed assembly, the last oligo is the reverse complement A
                    oligos_dict["A"].append(dfr.reverse_complement(identified_oligo))
            # else invalid path, end this recursion anyway
            continue

        # reached a position where an overhang is expected, check and if correct save the oligo and continue, else stop this recursion
        if len(total_path) in overhangs_end_position.keys():
            step_reached = overhangs_end_position[len(total_path)]
            if not reversed:
                expected_overhang = overhangs_list[step_reached]
            else:
                step_reached = 9-step_reached # count from 8 to 0
                expected_overhang = dfr.reverse_complement(overhangs_list[step_reached-1])

            if total_path.endswith(expected_overhang): # the expected overhang is found, then the builded oligo is likely to be correct
                #print("consensus on the good way")
                oligo_found_name = oligo_names[step_reached]

                identified_oligo = total_path[-(oligo_sizes[step_reached]+4):-4] # extract the oligo from the path minus the 4 overhang bases
                # save the identified oligo
                if not reversed:
                    oligos_dict[oligo_found_name].append(identified_oligo)
                else:
                    oligos_dict[oligo_found_name].append(dfr.reverse_complement(identified_oligo))
            else: # going in an incorrect way, end the research from this path
                #print("wrong way")
                continue

        last_added_kmer = total_path[-kmer_size:]

        if len(total_path)-kmer_size in overhangs_end_position.keys():
            # the current kmer is the start of a new oligo, just after the previous reached overhang
            # test if it is the checkspoints, if then it means the consensus for this oligo has already been researched
            # then end the search from this path as it would be useless

            if check_points_kmers.get(last_added_kmer, False):
                #print("known checkpoint")
                continue
            else:
                # else add this checkpoint
                #print("new checkpoint")
                check_points_kmers[last_added_kmer] = True

        # optimisation
        # when the path arrives to an oligo transition, reduce the length of kmers since a transition between 2 specific oligos is rare, so it has less long kmers
        critical_position = False
        for critic_length in range(kmer_size-5): # look at positions just after the overhang until the start of the kmer is after the overhang
            if len(total_path)-critic_length in overhangs_end_position.keys():
                critical_position = True
                break

        # get the occurrence score for each 4 possible next overlapping kmers
        for next_base in ["A","C","G","T"]:

            if critical_position:
                # next kmer is short, and then is searched in the short kmers occurrences dict
                next_kmer = total_path[-short_kmer_size+1:]+next_base
                next_weight = short_kmer_occurrences_dict.get(next_kmer, 0)
            else:
                next_kmer = last_added_kmer[1:]+next_base
                next_weight = kmer_occurrences_dict.get(next_kmer, 0)

            # continue the path in up to 4 directions if the next voerlapping kmers have a minimum number of occurrences in the reads
            if next_weight > 0:
                #print(last_added_kmer + " " +next_base+":"+str(next_weight)+" ("+str(len(total_path))+")")
                path_stack.append(total_path + next_base)

    return oligos_dict


def kmer_consensus(input_path: str, output_path: str, start_primer: str, stop_primer: str, kmer_size: int, min_occ: int) -> None:
    """
    get the consensus sequence from an ordered fragments assembly
    find a list of overlapping kmers from the dict of kmer occurrences
    build the original sequence from the start_primer and stop when matching the stop_primer    
    """

    print("kmer counting...")
    # count the occurrences of kmers in the reads
    global kmer_occurrences_dict
    kmer_occurrences_dict = count_kmers(input_path, kmer_size, min_occ)

    # count the occurrences of shorter kmers
    short_kmer_size = max(12, kmer_size-8)
    global short_kmer_occurrences_dict
    short_kmer_occurrences_dict = count_kmers(input_path, short_kmer_size, min_occ)

    print()

    print("graph building in both ways...")

    oligos_dict = compute_all_consensus(kmer_size, short_kmer_size, start_primer, dfr.reverse_complement(stop_primer))

    print([name+"="+str(len(k)) for name, k in oligos_dict.items()])

    # compute the consensus again in reverse order, starting to the end_primer to get to the start primer
    # the consensus usually find more oligos at the beginning of the path, so the reverse consensus helps to find more oligos
    oligos_dict_rv = compute_all_consensus(kmer_size, short_kmer_size, stop_primer, dfr.reverse_complement(start_primer), reversed=True)

    print([name+"="+str(len(k)) for name, k in oligos_dict_rv.items()])

    print("total different oligos found")
    total_oligo_dict = {}

    # merge all found oligos
    for oligo_name in oligo_names:
        count = 0
        sub_dict = {}
        for oligo_seq in (oligos_dict[oligo_name] + oligos_dict_rv[oligo_name]):
            if not oligo_seq in list(sub_dict.values()):
                sub_dict[oligo_name+"_"+str(count)] = oligo_seq
                count += 1
        total_oligo_dict = total_oligo_dict | sub_dict
        print(oligo_name+" : "+str(count))

    # save every unique oligo to output
    dfr.save_dict_to_fasta(total_oligo_dict, output_path)


if __name__ == "__main__":

    """
    parser = argparse.ArgumentParser(description='get the original complete sequence from sequencing reads of ordered assembly')
    parser.add_argument('-i', action='store', dest='input_path', required=True,
                        help='fastq file to read the sequences')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='fasta file to save the result')
    parser.add_argument('--start', action='store', dest='start_primer', required=True,
                        help='start primer at the beginning of the sequence to reconstruct')
    parser.add_argument('--stop', action='store', dest='stop_primer', required=True,
                        help='stop primer at the beginning of the reverse complement of the sequence to reconstruct')
    parser.add_argument('--kmer_size', action='store', dest='kmer_size', required=False,
                        default=20, type=int, help='length of the kmers searched, arbitrary constant, increase the risk of finding loop if too short')
    parser.add_argument('--min_occ', action='store', dest='min_occ', required=False,
                        default=2, type=int, help='minimum occurrences for kmers to be used in the consensus')
    
    arg = parser.parse_args()
    """

    print("semi-ordered assembly consensus...")

    #kmer_consensus(arg.input_path, arg.output_path, arg.start_primer, arg.stop_primer, arg.kmer_size, arg.min_occ, arg.seq_siz)

    kmer_consensus("decoding_container/simu_reads_g5.fastq", "simu_consensus_g5.fasta", "TCCTTGACAGCGGAAGATAC", "ATCTAACGCAGGGACTTAGC", 20, 2)

    print("\tcompleted !")

