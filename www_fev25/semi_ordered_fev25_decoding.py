#!/usr/bin/python3

import os
import sys
import inspect
import math
import subprocess
import random #TODO remove after consensus implementation

import semi_ordered_fev25 as sof
import semi_ordered_consensus as soc
import semi_ordered_assembly_simulation as soas

currentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
import primers_generation_V2 as pg

sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import file_to_dna as ftd



def extract_fragments(output_consensus_path, index_size):
    """
    convert the consensus oligos to a dict of index:binary sequence
    extract the dna payload, convert to binary, extract and test the checksum, extract the index
    """

    consenus_oligos_dict = dfr.read_fasta(output_consensus_path) # get oligos found from the consensus
    indexed_retrieved_frags_dict = {} # dict : key = fragment index, value = binary sequence

    for name, seq in consenus_oligos_dict.items():

        extracted_seq = seq[1:-1] # remove 2 added bases surrounding the dna payload

        # convert to binary
        extracted_binary = ftd.bdc.dna_to_binary_baa(extracted_seq)

        # extremities oligos have shorter checksum
        if "A" in name or "J" in name:
            checksum_size = 18
        else:
            checksum_size = 33

        check_sum = extracted_binary[-checksum_size:]
        bin_wo_checksum = extracted_binary[:-checksum_size]

        # test if checksum is valid
        if sof.compute_check_sum(bin_wo_checksum, checksum_size) == check_sum:
            # extract the binary index and convert to int
            index_frag = int(bin_wo_checksum[:index_size], 2)

            #print(seq[:m], index_frag)
            extracted_frag = bin_wo_checksum[index_size:]

            if indexed_retrieved_frags_dict.get(index_frag, False):
                print(name)
            indexed_retrieved_frags_dict[index_frag] = extracted_frag


    print("correct extracted fragments : "+str(len(indexed_retrieved_frags_dict))+" out of "+str(len(consenus_oligos_dict)))

    return indexed_retrieved_frags_dict


def decode_reed_solomon(indexed_retrieved_frags_dict: list, K: int, m: int, n_sub: int, group_number: int) -> list:

    # test if enough fragments have been retrieved
    if len(indexed_retrieved_frags_dict) < K:
        print("Decoding fail, not enough fragments retrieved ("+str(len(indexed_retrieved_frags_dict))+") required at least "+str(K))
        exit(1)

    retrieved_fragments_list = [0]*(2**m-1) # create an empty list of N fragments
    for i in range(K):
        retrieved_i_frag_index = list(indexed_retrieved_frags_dict.keys())[i] # get the fragment index
        retrieved_i_frag_bin = indexed_retrieved_frags_dict[retrieved_i_frag_index] # get the associated fragment binary
        retrieved_fragments_list[retrieved_i_frag_index] = retrieved_i_frag_bin # store it at the correct position


    # execute the julia script to decode with Reed Solomon
    RS_write_script_path = "../rs-codes/storage_scripts/www_fev25_read.jl"

    encoded_bin_path = "decoding_container/RS_encoded_frags_group_"+str(group_number+1)+".binary"
    decoded_bin_path = "decoding_container/decoded_frags_group_"+str(group_number+1)+".binary"

    # write the binary to a file
    with open(encoded_bin_path, 'w') as output:
        for bin_frag in retrieved_fragments_list:
            if bin_frag == 0:
                output.write("\n") # write empty line if no retrieved fragment at this index
            else:
                output.write(bin_frag+"\n")

    #TODO if making another storage
    #print("#TODO skipping julia decoding this time")

    # call the julia script 
    julia_args = [str(K), str(m), str(n_sub), encoded_bin_path, decoded_bin_path]
    command = ["julia", RS_write_script_path] + julia_args
    result = subprocess.run(command, capture_output=True, text=True)
    if result.returncode != 0:
        print("Julia Error:", result.stderr)
        return 0

    # read the julia output
    with open(decoded_bin_path, 'r') as input:
        lines = input.readlines()
        decoded_bin_list = [line.replace("\n","") for line in lines]

    return decoded_bin_list


def convert_bits_to_file(binary_payload, output_path):

    # remove octets of zeros at the beginning (the start of the sequence can be filled with zeros to get a round number of blocks)
    while binary_payload.startswith(8*"0"): # 1/256 (2**8) chance to remove actual data ! but 8*0 is ascii char NULL
        binary_payload = binary_payload[8:]

    # convert binaries into bytes
    n = int(binary_payload, 2)
    bytes = n.to_bytes((n.bit_length() + 7) // 8, 'big')

    # write the bytes into the file
    with open(output_path, "wb") as f:
        f.write(bytes)


def semi_ordered_decoding():

    if not os.path.exists("decoding_container"):
        os.makedirs("decoding_container")

    group_number_list = [0, 1, 2, 3, 4]#

    # each group has a different K value
    groups_K_value = [1253, 1253, 1251, 1293, 1293]

    total_group_bin_payload = ""

    # primers used for assembly for each group
    assembly_primers = dfr.read_fasta("steps_container/assembly_primers.fasta")

    for group_number in group_number_list:

        K = groups_K_value[group_number]
        m, n_sub = 11, 21 # constants of the encoding, m because we stored 2**m-1 fragments in each group, n_sub because each binary frag encoded with RS is m*n_sub long

        #TODO simulation of the reads, to remove when real data
        group_reads_path = "decoding_container/simu_reads_g"+str(group_number+1)+".fastq"
        soas.simu_assembly("steps_container/all_final_oligos.fasta", group_number, 20000, group_reads_path)

        # consensus on reads
        output_consensus_path = "decoding_container/consensus_reads_g"+str(group_number+1)+".fasta"

        start_primer = next( primer for name, primer in assembly_primers.items() if name.startswith("G"+str(group_number+1)+"_start_primer"))
        stop_primer =  next( primer for name, primer in assembly_primers.items() if name.startswith("G"+str(group_number+1)+"_stop_primer"))

        soc.kmer_consensus(group_reads_path, output_consensus_path, start_primer, stop_primer, 22, 2)

        # extract consensus results to indexed binary fragments
        retrieved_fragments_list = extract_fragments(output_consensus_path, m)

        # apply the Reed Solomon decoding to get the original K fragments
        print("julia RS decoding ...")
        decoded_bin_list = decode_reed_solomon(retrieved_fragments_list, K, m, n_sub, group_number)

        binary_payload = "".join(decoded_bin_list)
        # reapply the filter to get the unfiltered data payload

        total_group_bin_payload += binary_payload

    filtered_bin_payload = ftd.apply_binary_filter(total_group_bin_payload)
    convert_bits_to_file(filtered_bin_payload, "decoding_container/group_"+str(group_number_list)+".jpg")


if __name__ == "__main__":

    print("semi ordered decoding...")

    semi_ordered_decoding()

    print("\tcompleted !")

