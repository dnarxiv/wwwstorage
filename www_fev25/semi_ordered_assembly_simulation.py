
import os
import sys
import inspect
import random

currentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr


oligo_names = "ABCDEFGHIJ"
i_error, d_error, s_error = 0.005, 0.01, 0.005 # ~2% error rate


def add_errors(sequence: str) -> str:
    """
    add the errors to the sequence
    :param line: line corresponding to the sequence
    :param i_error: insertion error rate
    :param d_error: deletion error rate
    :param s_error: substitution error rate
    :return: the line containing the errors
    """
    #return sequence
    synt_sequence = ""
    for letter in sequence:
        if random.random() < s_error:  # substitution error
            alphabet = ["A", "G", "C", "T"]
            alphabet.remove(letter)
            synt_sequence += random.choice(alphabet)
        else:
            synt_sequence += letter
        if random.random() < i_error:  # insertion error
            synt_sequence += random.choice(["A", "G", "C", "T"])
        if random.random() < d_error:  # deletion error
            synt_sequence = synt_sequence[:-1]

    return synt_sequence


def simu_assembly(oligo_path: str, group_number: int, assembly_nbr: int, output_path):

    oligo_dict = dfr.read_fasta(oligo_path)

    oligo_g1_dict = {name: seq for name, seq in oligo_dict.items() if "_g"+str(group_number+1)+"_" in name}

    simu_assembly_dict = {}

    nbr_different_oligo = 205
    oligo_set_dict = {}
    for j in range(10):
        oligo_set_dict[j] = {name: seq for name, seq in oligo_g1_dict.items() if "_"+oligo_names[j] in name and int(name.split(oligo_names[j])[1]) <= nbr_different_oligo }

    for i in range(assembly_nbr):

        assembled_oligo_names = [str(i)]
        assembled_oligo_seqs = []

        for j in range(10):
            oligo_set = oligo_set_dict[j]
            added_oligo = random.choice(list(oligo_set.items()))
            assembled_oligo_names.append(added_oligo[0].split("_")[2])
            assembled_oligo_seqs.append(added_oligo[1])

        assembled_sequences = assembled_oligo_seqs[0][20:202] + "".join(seq[31:202] for seq in assembled_oligo_seqs[1:9]) + assembled_oligo_seqs[9][31:209]

        # add synthesis errors and save to the dict
        simu_assembly_dict[str(i)+"_"+"_".join(assembled_oligo_names)] = add_errors(assembled_sequences)

    dfr.save_dict_to_fastq(simu_assembly_dict, output_path)


if __name__ == "__main__":

    simu_assembly("steps_container/all_final_oligos.fasta", 0, 20000, "simu_assembly.fastq")
