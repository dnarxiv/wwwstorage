#!/usr/bin/python3

import os
import random
import sys
import inspect
import math
import subprocess

currentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
import primers_generation_V2 as pg
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import sequence_control as sc
import file_to_dna as ftd


# restrition site used to digest overhangs only on one side
bsaI = "GGTCTCG"

blocks_names = "ABCDEFGHIJ" #

class oligo_block:

    def __init__(self, num: int, mol_id: int, payload: str):
        self.num = num
        self.payload = payload
        self.mol_id = mol_id
        self.start_primer_pool = ""
        self.stop_primer_pool = ""
        self.start_primer_assembly = "" # only added for blocks A
        self.stop_primer_assembly = "" # only for blocks J
        self.overhang_start = ""
        self.overhang_stop = ""


    def get_framed_primers(self):
        """
        payload framed by the assembly primers
        only equal to the payload for all blocks but the first and last one
        """
        sequence = self.start_primer_assembly + self.payload + dfr.reverse_complement(self.stop_primer_assembly)
        return sequence


    def get_framed_sites(self):
        """
        sequence between the pool primers
        also add if exists the assembly primers (only for 1st or last)
        """
        sequence = self.get_framed_primers()
        if self.overhang_start != "":
            sequence = bsaI + self.overhang_start + sequence
        if self.overhang_stop != "":
            sequence = sequence + self.overhang_stop + dfr.reverse_complement(bsaI)

        return sequence


    def get_complete_block(self):
        # complete block sequence
        return self.start_primer_pool + self.get_framed_sites() + dfr.reverse_complement(self.stop_primer_pool)


class Protocol_molecule:
    """
    class that manage a list of 10 oligos A..J
    used to format the 10 payloads into 10 complete oligos that can be assembled together
    """

    def __init__(self, mol_group: int, mol_id: int, intern_overhangs_list: list, payload_list: list):
        self.mol_group = mol_group
        self.mol_id = mol_id
        self.blocks_list = []
        for num, payload in enumerate(payload_list):
            block = oligo_block(num, mol_id, payload)
            self.blocks_list.append(block)

        self.assign_overhangs(intern_overhangs_list)



    def assign_overhangs(self, intern_overhangs_list: list):
        """
        assign a couple of overhangs for each blocks
        """
        self.blocks_list[0].overhang_stop = intern_overhangs_list[0]
        self.blocks_list[9].overhang_start = intern_overhangs_list[8]

        for i in range(1, 9):
            self.blocks_list[i].overhang_start = intern_overhangs_list[i - 1]
            self.blocks_list[i].overhang_stop = intern_overhangs_list[i]



    def assign_assembly_primers(self, Fw_primer: str, Rv_primer: str):
        """
        assign defined extern primers to the assembled molecules
        start primer for first block and stop primer to last block
        """
        self.blocks_list[0].start_primer_assembly = Fw_primer
        self.blocks_list[-1].stop_primer_assembly = Rv_primer


    def assign_pool_primers(self, Fw_primer: str, Rv_primer: str):
        """
        assign the primers used to amplify the pool
        """
        for block in self.blocks_list:
            block.start_primer_pool = Fw_primer
            block.stop_primer_pool = Rv_primer


    def get_sequences_with_stop_overhang(self) -> list:
        """
        get list of payload + stop overhang
        """
        overhang_sequences_list = []
        for block in self.blocks_list:
            seq_with_stop_overhang = block.payload + block.overhang_stop
            overhang_sequences_list.append(seq_with_stop_overhang)
        return overhang_sequences_list


    def get_blocks_with_sites(self) -> list:
        """
        get list of blocks with assembly primers and full restriction sites
        """
        sites_sequences_list = []
        for block in self.blocks_list:
            sites_sequences_list.append(block.get_framed_sites())
        return sites_sequences_list


    def save_blocks(self):
        """
        save all construction steps in fasta files
        """
        molecule_dir_path = "steps_container/group_"+self.mol_group+"/mol_"+str(self.mol_id + 1)
        if not os.path.exists(molecule_dir_path):
            os.makedirs(molecule_dir_path)

        blocks_payload_dict = {}
        blocks_assembly_primers_dict = {}
        blocks_ov_dict = {}
        blocks_with_pool_primers_dict = {}
        

        for i in range(10):
            block_name = "oligo_g"+ self.mol_group + "_" + blocks_names[i] + str(self.mol_id +1)

            block = self.blocks_list[i]
            blocks_payload_dict[block_name] = block.payload
            blocks_assembly_primers_dict[block_name] = block.get_framed_primers()
            blocks_ov_dict[block_name] = block.get_framed_sites()
            blocks_with_pool_primers_dict[block_name] = block.get_complete_block()


        dfr.save_dict_to_fasta(blocks_payload_dict, molecule_dir_path+"/0_payload.fasta")
        dfr.save_dict_to_fasta(blocks_assembly_primers_dict, molecule_dir_path+"/1_assembly_primers.fasta")
        dfr.save_dict_to_fasta(blocks_ov_dict, molecule_dir_path+"/2_overhangs.fasta")
        dfr.save_dict_to_fasta(blocks_with_pool_primers_dict, molecule_dir_path+"/3_pool_primers.fasta")


def split_and_filter_binary(binary_payload: str, part_size: int) -> list:
    """
    split the binary into equal parts of fixed size, complete with '0' if not a round number
    add a binary filter to the data
    """
    number_of_assembly = len(binary_payload) / part_size

    if number_of_assembly != int(number_of_assembly): # not a round number
        print(str(number_of_assembly)+" assemblies, need to complete")
        filler_length = math.ceil(number_of_assembly) * part_size - len(binary_payload)
        # fill with '0' at the beginning of the binary # not the end because some zip can end with octets of 0, which makes difficult to remove only the non coding '0'
        binary_payload = filler_length * "0" + binary_payload

    # apply a binary filter to the full payload to give 'randomized' form to the binaries
    filt_binary_payload = ftd.apply_binary_filter(binary_payload)

    number_of_assembly = math.ceil(number_of_assembly)

    # split the fragments in fixed sizes
    binary_fragments = [filt_binary_payload[i*part_size:(i+1)*part_size] for i in range(number_of_assembly)]

    return binary_fragments


def add_index_to_binary(binary_fragments: list, bin_size: int) -> list:
    """
    add a binary index of defined size before each fragment
    """
    indexed_fragments = []

    for i, frag in enumerate(binary_fragments):
        bin_index = bin(i)[2:].zfill(bin_size) # convert number to bin of fixed size
        indexed_fragments.append(bin_index+frag)

    if len(bin_index) > bin_size:
        print("error, bin index too large "+bin_index+" ("+str(i)+")")
        exit(1)
    return indexed_fragments


def apply_reed_solomon(binary_fragments: list, nbr_groups: int, m: int, n_sub: int, first_group_num: int) -> list:
    """
    split the list into groups with odd number of fragments, each one is encoded with a Reed Solomon code to reach a defined amount of fragments
    the RS code needs K = odd fragments, of length = n_sub * m to encode them to 2**m -1 fragments
    """
    # split the list into <nbr_groups> groups of odd number of fragments
    frags_num = len(binary_fragments)
    base_size = (frags_num // nbr_groups) | 1 # create odd number of base fragments

    splited_lists = [binary_fragments[i * base_size:(i + 1) * base_size] for i in range(nbr_groups-1)]
    splited_lists += [binary_fragments[(nbr_groups-1) * base_size:]]

    # execute the julia script to encode with Reed Solomon for each group
    RS_write_script_path = "../rs-codes/storage_scripts/www_fev25_write.jl"

    encoded_splited_lists = []
    for i, split_list in enumerate(splited_lists):

        K = len(split_list)
        print("Applying Reed Solomon encoding on group of K="+str(K)+" to N="+str(2**m-1))
        if K // 2 == 0:
            print("Split Error, K is not odd")
            exit(1)

        saved_bin_path = "steps_container/group_"+str(first_group_num+i)+"/base_frags.binary"
        encoded_bin_path = "steps_container/group_"+str(first_group_num+i)+"/RS_frags.binary"

        # write the binary to a file
        with open(saved_bin_path, 'w') as output:
            for bin_frag in split_list:
                output.write(bin_frag+"\n")

        # call the julia script
        julia_args = [str(K), str(m), str(n_sub), saved_bin_path, encoded_bin_path]
        command = ["julia", RS_write_script_path] + julia_args
        result = subprocess.run(command, capture_output=True, text=True)
        if result.returncode != 0:
            print("Julia Error:", result.stderr)
            return 0

        # read the julia output
        with open(encoded_bin_path, 'r') as input:
            lines = input.readlines()
            encoded_bin_list = [line.replace("\n","") for line in lines]

        encoded_splited_lists.append(encoded_bin_list)

    return encoded_splited_lists


def frame_frag(dna_frag: str) -> str:
    """
    add 2 non coding bases around the fragments
    used to break involontary homopolymeres or bsaI sites
    that could be created when joining the payload with a restriction site or primer
    """
    if dna_frag[0] == "C":
        dna_frag = "G" + dna_frag
    elif dna_frag[0] == "T":
        dna_frag = "A" + dna_frag
    elif dna_frag[0] == "A":
        dna_frag = random.choice(["T", "C"]) + dna_frag
    else: # == G
        dna_frag = random.choice(["T", "C"]) + dna_frag

    if dna_frag[-1] == "C":
        dna_frag = dna_frag + random.choice(["A","G"])
    elif dna_frag[-1] == "T":
        dna_frag = dna_frag + random.choice(["A", "G"])
    elif dna_frag[-1] == "A":
        dna_frag = dna_frag + "T"
    else: # == G
        dna_frag = dna_frag + "C"
    return dna_frag


def get_compatible_assembly_primers(protocol_molecule_list: list, concatenations_path: str, other_protocols_primers_dict: dict) -> dict:
    """
    get a pair of primers compatible with the assembly of any blocks within
    """
    list_of_oligos = [] # list of molecules oligos with the stop overhang

    for molecule in protocol_molecule_list:
        list_of_oligos.append(molecule.get_sequences_with_stop_overhang())

    dict_of_possible_concatenations = {}
    # create every possible concatenation between a block N and block N+1 from any molecules
    # not working for large protocols, too much possibility of concatenations for the primer script
    """
    for mol_id in range(len(protocol_molecule_list)):
        for block_num in range(9):
            first_concat_part = list_of_oligos[mol_id][block_num]
            for other_mol_id in range(len(protocol_molecule_list)):
                second_concat_part = list_of_oligos[other_mol_id][block_num+1]
                dict_of_possible_concatenations["mol_"+str(mol_id+1)+blocks_names[block_num]+"+mol_"+str(other_mol_id+1)+blocks_names[block_num+1]] = first_concat_part + second_concat_part
    """
    # short version with only concats within the same mol
    for mol_id in range(len(protocol_molecule_list)):
        for i in range(9):
            concat_name = "mol_"+str(mol_id+1)+blocks_names[i]+"+mol"+str(mol_id+1)+blocks_names[i+1]
            dict_of_possible_concatenations[concat_name] = list_of_oligos[mol_id][i] + list_of_oligos[mol_id][i+1]

    dfr.save_dict_to_fasta(dict_of_possible_concatenations, concatenations_path)

    # generate 2000 primers compatible with the defined parameters in the generator script
    pg.generate_compatible_primers(2000)

    # removes the primers that hybridize with the concatenations
    pg.filter_primers_with_oligos(concatenations_path)

    best_pair_dict = pg.get_best_compatible_pair(other_primers_dict = other_protocols_primers_dict)

    return best_pair_dict


def get_compatible_pool_primers(group_protocols_list, mol_with_sites_path, assembly_primers_couple) -> dict:
    """
    get a pair of primers compatible for the amplification of every blocks
    """
    mol_with_sites_dict = {}
    for group in group_protocols_list:
        for molecule in group:
            blocks_list = molecule.get_blocks_with_sites()
            for i, block in enumerate(blocks_list):
                mol_with_sites_dict["group_"+molecule.mol_group+"_mol_"+str(molecule.mol_id)+"_"+str(i)] = block

    dfr.save_dict_to_fasta(mol_with_sites_dict, mol_with_sites_path)

    # generate 2000 primers compatible with the defined parameters in the generator script
    pg.generate_compatible_primers(2000)

    # removes the primers that hybridize with the oligos with sites
    pg.filter_primers_with_oligos(mol_with_sites_path)

    best_pair_dict = pg.get_best_compatible_pair(other_primers_dict = assembly_primers_couple)

    return best_pair_dict



def compute_check_sum(binary_string: str, checksum_size: int) -> str:
    """
    compute the check_sum binary value associated with the binary string
    """

    # split the string into substrings of checksum_size
    splitted_binary_string = [binary_string[i:i+checksum_size] for i in range(0, len(binary_string), checksum_size)]

    bin_sum = "0"
    for binary_split in splitted_binary_string:

        int_sum = int(bin_sum, 2) ^ int(binary_split,2) # XOR sum
        bin_sum = str(bin(int_sum))[2:] # convert back into binary

    return bin_sum.zfill(checksum_size) # fill the beginning with 0 to get the correct size


def binary_group_to_molecule(binary_group: list, group_name: str) -> list:
    """
    take a list of binary sequences
    assign each to an oligo A-J
    add amount of checksum depending on oligo letter
    convert to dna
    put in a molecule class
    """
    group_assembly_list = []

    # divide the fragments in lists of 10, with a checksum to each one
    for assembly_nbr in range(len(binary_group)// 10): # 10 frag per assembly
        assembly_list = []

        start_assembly_frag = binary_group[assembly_nbr*10]
        start_assembly_frag += compute_check_sum(start_assembly_frag, 18) # add bits of checksum, to fill the binary to 260 bits
        assembly_list.append(start_assembly_frag)

        for index in range((assembly_nbr*10)+1, (assembly_nbr*10)+9):
            mid_assembly_frags = binary_group[index]
            mid_assembly_frags += compute_check_sum(mid_assembly_frags, 33) # add bits of checksum, to fill the binary to 275 bits
            assembly_list.append(mid_assembly_frags)

        stop_assembly_frag = binary_group[(assembly_nbr*10)+9]
        stop_assembly_frag += compute_check_sum(stop_assembly_frag, 18) # add bits of checksum, to fill the binary to 260 bits
        assembly_list.append(stop_assembly_frag)

        group_assembly_list.append(assembly_list)

    # save the checksum fragments
    with open("steps_container/group_"+group_name+"/1_checksum_payload.binary", 'w') as output:
        for i, assembly_frags in enumerate(group_assembly_list):
            for j, frag in enumerate(assembly_frags):
                output.write(">assembly_"+blocks_names[j]+str(i)+"\n"+frag+"\n")


    # convert each part to dna sequence
    overhangs_list = list(dfr.read_fasta("overhangs_9.fasta").values())
    protocol_molecule_list = []

    for i, assembly_frags in enumerate(group_assembly_list):
        molecule_frag_list = []
        for j, bin_frag in enumerate(assembly_frags):
            dna_frag = ftd.bdc.binary_to_dna_baa(bin_frag)
            framed_dna_frag = frame_frag(dna_frag) # add 2 bases around the frag
            molecule_frag_list.append(framed_dna_frag)

        mol_i = Protocol_molecule(group_name, i, overhangs_list, molecule_frag_list)
        protocol_molecule_list.append(mol_i)

    return protocol_molecule_list



def generate_protocols_molecules() -> list:
    """
    create the molecules for the semi ordered manip
    """

    if not os.path.exists("steps_container"):
        os.makedirs("steps_container")

    # convert documents to binary
    binary_payload = ftd.convert_file_to_bits("WWW_input.tar.gz") # 108.5 kB of a compressed directory
    binary_payload_2 = ftd.convert_file_to_bits("9407011_31_compressed_52.jpg") # 74.7 kB of a picture

    # cut the binary into payload parts for the oligos
    binary_fragments = split_and_filter_binary(binary_payload, 231)
    binary_fragments_2 = split_and_filter_binary(binary_payload_2, 231)

    # add new redundant parts with Reed Solomon algorithm
    m, n_sub = 11, 21 # fragments must be m * n_sub long, and will be encoded to 2**m -1 fragments (2047)
    reed_solomon_group_list = apply_reed_solomon(binary_fragments, 3, m, n_sub, 1) # the dir is split in 3 groups
    reed_solomon_group_list_2 = apply_reed_solomon(binary_fragments_2, 2, m, n_sub, 4) # the picture is split in 2 groups

    total_list = reed_solomon_group_list + reed_solomon_group_list_2 # 5 groups of encoded binary fragments

    assembly_primers_dict = {} # dict of primers used in the assembly PCR
    group_protocols_list = [] # list of group molecules

    for group_num, binary_list in enumerate(total_list):

        indexed_binary_list = add_index_to_binary(binary_list, m) # add m bits of index to RS_encoded fragments, need the num of each fragment for decoding th RS algorithm

        # take the 3 first frag and duplicate it to have 2050 (multiple of 10)
        indexed_binary_list += indexed_binary_list[:3]

        # convert to a list of dna molecules, here a molecule reffers to 10 fragments A..J
        protocol_molecule_list = binary_group_to_molecule(indexed_binary_list, str(group_num + 1))
        group_protocols_list.append(protocol_molecule_list)

        # compute the assembly primers for this group
        possible_concat_path = "temp/concats_group_"+str(group_num+1)+".fasta"
        assembly_primers_couple = get_compatible_assembly_primers(protocol_molecule_list, possible_concat_path, assembly_primers_dict)

        start_primer_name, start_primer_seq = list(assembly_primers_couple.items())[0]
        stop_primer_name, stop_primer_seq = list(assembly_primers_couple.items())[1]

        # assign the assembly primers to the molecules
        for molecule in protocol_molecule_list:
            molecule.assign_assembly_primers(start_primer_seq, stop_primer_seq) # so add the start_primer to the A fragment and stop_primer to the J fragment
        # save the assembly primers
        dfr.save_dict_to_fasta(assembly_primers_couple, "steps_container/group_"+str(group_num+1)+"/G"+str(group_num+1)+"_assembly_primers.fasta")

        # add to the assembly dict
        assembly_primers_dict["G"+str(group_num+1)+"_start_primer : "+start_primer_name] = start_primer_seq
        assembly_primers_dict["G"+str(group_num+1)+"_stop_primer : "+stop_primer_name] = stop_primer_seq

    # save all the assembly primers in the ouput directory root
    dfr.save_dict_to_fasta(assembly_primers_dict, "steps_container/assembly_primers.fasta")

    # compute the oligos primers, same pair is used for every oligos in the manip
    mol_with_sites_path = "temp/all_oligos_with_restr_sites.fasta"
    oligo_primers_couple = get_compatible_pool_primers(group_protocols_list, mol_with_sites_path, assembly_primers_dict)
    oligo_primers_sequences = list(oligo_primers_couple.values())

    # assign the oligo primers and save all the construction steps
    for i, protocol_molecule_list in enumerate(group_protocols_list):
        for molecule in protocol_molecule_list:
            molecule.assign_pool_primers(oligo_primers_sequences[0], oligo_primers_sequences[1])
            # every step in molecule creation is saved for easier debugging
            molecule.save_blocks()

    # save the oligo primers
    dfr.save_dict_to_fasta(oligo_primers_couple, "steps_container/pool_primers.fasta")

    # put final molecules in 1 total file and 1 file for each group
    all_oligos_file = "steps_container/all_final_oligos.fasta"
    with open(all_oligos_file, 'w') as outfile:
        for i in range(len(group_protocols_list)):
            with open("steps_container/G_"+str(i+1)+"final_oligos.fasta", 'w') as outfile_group:
                for mol_num in range(205):
                    final_mol_path = "steps_container/group_"+str(i+1)+"/mol_"+str(mol_num+1)+"/3_pool_primers.fasta"
                    with open(final_mol_path, 'r') as infile:
                        group_sequences = infile.read()
                        outfile.write(group_sequences)
                        outfile_group.write(group_sequences)



def final_check(all_oligos_file):
    """
    read all generated sequences and test for homopolymeres & unwanted bsaI restriction sites
    """

    all_oligo_dict = dfr.read_fasta(all_oligos_file)
    h_max_size = 5 # maximum homopolymere size

    for oligo_name, sequence in all_oligo_dict.items():
        n_bsaI = sc.get_forbidden_sequences_nbr(sequence)
        # block A & J have 1 bsaI, others have 2
        if (n_bsaI > 2 and not ("_A" in sequence or "_J" in sequence)) or (n_bsaI > 1 and ("_A" in sequence or "_J" in sequence)):
            print("bsaI found !")
            print(oligo_name)
            print(sequence)
            exit(0)
        if h_max_size*"A" in sequence or h_max_size*"T" in sequence or h_max_size*"C" in sequence or h_max_size*"G" in sequence:
            #continue
            print("homopolymere found !")
            print(oligo_name)
            print(sequence)#[22:-22])

    print("final check completed")

    #template_for_idt_test(sum_of_blocks_dict, "steps_container/all_oligo_idt.xlsx")



if __name__ == "__main__":
    
    print("semi ordered encoding...")

    generate_protocols_molecules()
    final_check("steps_container/all_final_oligos.fasta")

    print("\tcompleted !")

