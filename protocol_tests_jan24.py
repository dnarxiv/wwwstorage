#!/usr/bin/python3

import os
import sys
import inspect
import argparse
import xlsxwriter

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
import eBlocks_design
import primers_generation as pg
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import sequence_control as sc


# restrition site used to digest overhangs only on one side
bsaI = "GGTCTCG"


class eBlock:

    def __init__(self, num: int, payload: str):
        self.num = num
        self.payload = payload
        self.start_primer_eBlock = ""
        self.stop_primer_eBlock = ""
        self.start_primer_mol = "" # only added for blocks 1 & 6
        self.stop_primer_mol = "" # only for blocks 4 & 9
        self.overhang_start = ""
        self.overhang_stop = ""

    def get_framed_primers(self):
        # start_primer_mol (if exists) + payload + stop_primer_mol (if exists)
        return self.start_primer_mol + self.payload + dfr.reverse_complement(self.stop_primer_mol)

    def get_framed_bsaI(self):
        # sequence between the eBlocks primers
        return bsaI + self.overhang_start + self.get_framed_primers() + self.overhang_stop + dfr.reverse_complement(bsaI)


class Protocol_molecule:

    def __init__(self, name: str, intern_overhangs_list: list, extern_overhangs_list: list, payload_list: list) -> None:
        self.name = name
        self.blocks_dict = {}
        for num, payload in enumerate(payload_list):
            block = eBlock(num, payload)
            self.blocks_dict[num] = block

        self.assign_overhangs(intern_overhangs_list, extern_overhangs_list)


    def assign_overhangs(self, intern_overhangs_list, extern_overhangs_list):
        """
        assign a couple of overhangs for each blocks
        """
        self.blocks_dict[0].overhang_start = extern_overhangs_list[0]
        self.blocks_dict[0].overhang_stop = intern_overhangs_list[0]

        self.blocks_dict[8].overhang_start = intern_overhangs_list[7]
        self.blocks_dict[8].overhang_stop = extern_overhangs_list[1]

        for i in range(1, 8):
            self.blocks_dict[i].overhang_start = intern_overhangs_list[i-1]
            self.blocks_dict[i].overhang_stop = intern_overhangs_list[i]


    def generate_intern_primers(self):
        """
        create primers to amplifie the whole eBlock
        """
        for i in range(9):
            block = self.blocks_dict[i]
            block_sequence = block.get_framed_bsaI()
            compatible_primers_list = pg.generate_compatible_primers(block_sequence)
            # create 2 primers at extremities
            selected_primers_list = pg.select_primers(compatible_primers_list, 2)
            block.start_primer_eBlock = selected_primers_list[0]
            block.stop_primer_eBlock = selected_primers_list[1]

        self.save_primers()


    def assign_mol_primers(self, Fw_primer, Rv_primer_A, Fw_primer_B, Rv_primer):
        """
        assign defined extern primers to the molecule
        """
        self.blocks_dict[0].start_primer_mol = Fw_primer
        self.blocks_dict[3].stop_primer_mol = Rv_primer_A
        self.blocks_dict[5].start_primer_mol = Fw_primer_B
        self.blocks_dict[8].stop_primer_mol = Rv_primer


    def save_primers(self):
        """
        write in file the generated primers for the eBlocks
        """
        primers_dict = {}
        for block in self.blocks_dict.values():
            primer_name = self.name+"_Fw_eBlock_0"+str(block.num+1)
            primers_dict[primer_name] = block.start_primer_eBlock
            primer_name = self.name+"_Rv_eBlock_0"+str(block.num+1)
            primers_dict[primer_name] = block.stop_primer_eBlock
        dfr.save_dict_to_fasta(primers_dict, "protocol_jan24/molecules/"+self.name+"/eBlock_primers.fasta")


    def get_concatened_sequence(self):
        """
        get what the assembled molecule will be between the 2 molecule primers
        """
        concatened_sequence = ""
        for i in range(8):
            block = self.blocks_dict[i]
            concatened_sequence += block.payload + block.overhang_stop
        concatened_sequence += self.blocks_dict[8].payload

        return concatened_sequence


    def save_blocks(self):
        """
        save all construction steps in fasta files
        """
        if not os.path.exists("protocol_jan24/molecules/"+self.name):
            os.makedirs("protocol_jan24/molecules/"+self.name)

        blocks_payload_dict = {}
        blocks_mol_primers_dict = {}
        blocks_ov_dict = {}
        blocks_bsaI_dict = {}
        blocks_eBlock_primers_dict = {}

        for i in range(9):
            block_name = self.name + "_eBlock_0"+str(i+1)
            block = self.blocks_dict[i]
            blocks_payload_dict[block_name] = block.payload
            blocks_mol_primers_dict[block_name] = block.get_framed_primers()
            blocks_ov_dict[block_name] = block.overhang_start + block.get_framed_primers() + block.overhang_stop
            blocks_bsaI_dict[block_name] = block.get_framed_bsaI()
            blocks_eBlock_primers_dict[block_name] = block.start_primer_eBlock + blocks_bsaI_dict[block_name] + dfr.reverse_complement(block.stop_primer_eBlock)
        blocks_buffer_dict = eBlocks_design.add_buffer(blocks_eBlock_primers_dict)

        dfr.save_dict_to_fasta(blocks_payload_dict, "protocol_jan24/molecules/"+self.name+"/0_payload.fasta")
        dfr.save_dict_to_fasta(blocks_mol_primers_dict, "protocol_jan24/molecules/"+self.name+"/1_mol_primers.fasta")
        dfr.save_dict_to_fasta(blocks_ov_dict, "protocol_jan24/molecules/"+self.name+"/2_overhangs.fasta")
        dfr.save_dict_to_fasta(blocks_bsaI_dict, "protocol_jan24/molecules/"+self.name+"/3_bsaI.fasta")
        dfr.save_dict_to_fasta(blocks_eBlock_primers_dict, "protocol_jan24/molecules/"+self.name+"/4_eBlock_primers.fasta")
        dfr.save_dict_to_fasta(blocks_buffer_dict, "protocol_jan24/molecules/"+self.name+"/5_buffer.fasta")


def template_for_idt_test(sum_of_blocks_dict, output_path: str):
    """
    write the blocks in a table to test the validity on idt website
    https://eu.idtdna.com/site/order/plate/eblocks
    """
    molecules_blocks_list = list(sum_of_blocks_dict.items())
    
    workbook = xlsxwriter.Workbook(output_path)
    worksheet = workbook.add_worksheet()
    
    worksheet.write(0, 0, "Well Position")
    worksheet.write(0, 1, "Name")
    worksheet.write(0, 2, "Sequence")
    #output.write("Well Position;Name;Sequence\n")
    
    mol_count = 0
    for i in ["A","B","C","D","E","F","G","H", "I", "J", "K", "L", "M", "N", "O"]:
        for j in range(1,10):
            molecule = molecules_blocks_list[mol_count]
            mol_count += 1
            #output.write(i+str(j)+";"+molecule[0]+";"+molecule[1]+"\n")
            worksheet.write(mol_count, 0, i+str(j))
            worksheet.write(mol_count, 1, molecule[0])
            worksheet.write(mol_count, 2, molecule[1])
            if mol_count >= len(molecules_blocks_list):
                workbook.close()
                return
    


def generate_protocole_molecules() -> list:
    
    overhangs_list = list(dfr.read_fasta("protocol_jan24/overhangs_40.fasta").values())

    if not os.path.exists("protocol_jan24/molecules"):
        os.makedirs("protocol_jan24/molecules")


    payload_path = "protocol_jan24/bit_caller.fasta"
    payload_dict = dfr.read_fasta(payload_path)

    framed_payload_dict = {}

    for i, (name, payload) in enumerate(payload_dict.items()):
        if i % 9 in [0,3,5,8]: # add 29 bases (15 + 14)
            payload = "GAAAGAAGGAGGAAG" + payload + "AGGAGAAAGAGGAA"
        else:
            # add 49 bases (25 + 24)
            payload = "AGAGGGAGGGAAGGAGAAGAAGAGG" + payload + "AGGAGAAAGAGGAAGGGAGAAGGA"
        framed_payload_dict[name] = payload

    dfr.save_dict_to_fasta(framed_payload_dict, "protocol_jan24/bit_caller_extended.fasta")

    framed_payload = list(framed_payload_dict.values())


    mol_1 = Protocol_molecule("mol_1", overhangs_list[0:8], overhangs_list[8:10], framed_payload[0:9])
    mol_2 = Protocol_molecule("mol_2", overhangs_list[10:18], overhangs_list[18:20], framed_payload[9:18])
    mol_3 = Protocol_molecule("mol_3", overhangs_list[20:28], overhangs_list[28:30], framed_payload[18:27])
    mol_4 = Protocol_molecule("mol_4", overhangs_list[30:38], overhangs_list[38:40], framed_payload[27:36])

    protocol_molecule_list = [mol_1, mol_2, mol_3, mol_4]
    """
    total_sequence = ""

    for molecule in protocol_molecule_list:
        total_sequence += molecule.get_concatened_sequence()


    print("\n"+str(len(total_sequence)))

    # generate primers compatible with the full joined sequence
    compatible_primers_list = pg.generate_compatible_primers(total_sequence)
    
    # select needed primers
    selected_primers_list = pg.select_primers(compatible_primers_list, 12)
    
    for p in selected_primers_list:
        print(p+" "+str(p.count("A")+p.count("T")))
    exit(0)
    """
    primers_list = list(dfr.read_fasta("protocol_jan24/mol_primers.fasta").values())

    for i, molecule in enumerate(protocol_molecule_list):
        print("primers for",molecule.name)
        molecule.assign_mol_primers(primers_list[4*i], primers_list[4*i+1], primers_list[4*i+2], primers_list[4*i+3])
        molecule.generate_intern_primers()
        molecule.save_blocks()

    # check and put all final molecules in 1 file
    final_check()



def final_check():

    sum_of_blocks_dict = {}

    for num in range(1,5):
        block_dict_i = dfr.read_fasta("protocol_jan24/molecules/mol_"+str(num)+"/5_buffer.fasta")

        for block_name, sequence in block_dict_i.items():
            n_bsaI = sc.get_forbidden_sequences_nbr(sequence)
            if n_bsaI > 2:
                print("bsaI found !")
                print(block_name)
                print(sequence)
                exit(0)
            if 4*"A" in sequence or 4*"T" in sequence or 4*"C" in sequence or 4*"G" in sequence:
                continue
                print("homopolymere found !")
                print(block_name)
                print(sequence[22:-22])

        sum_of_blocks_dict.update(block_dict_i) # concat the dicts

    dfr.save_dict_to_fasta(sum_of_blocks_dict, "protocol_jan24/molecules/all_eBlocks.fasta")

    template_for_idt_test(sum_of_blocks_dict, "protocol_jan24/molecules/all_eBlocks_idt.xlsx")

    
if __name__ == "__main__":
    
    print("protocol tests...")
    
    protocol_molecule_list = generate_protocole_molecules()
                
    print("\tcompleted !")

