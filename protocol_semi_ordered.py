#!/usr/bin/python3

import os
import sys
import inspect
import argparse

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
import eBlocks_design
import primers_generation_V2 as pg
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import sequence_control as sc
import file_to_dna as ftd


# restrition site used to digest overhangs only on one side
bsaI = "GGTCTCG"

# unique id for each complete molecule parts
dna_mol_id_dict = {0:"AT", 1:"AC",2:"AG",3:"TA",4:"TG",5:"CA",6:"CT",7:"CG",8:"GT",9:"GC"}


class oligo_block:

    def __init__(self, num: int, mol_id: int, payload: str):
        self.num = num
        self.payload = dna_mol_id_dict[mol_id] + payload # add the mol index at the beginning
        self.mol_id = mol_id
        self.start_primer_pool = ""
        self.stop_primer_pool = ""
        self.start_primer_assembly = "" # only added for blocks X1
        self.stop_primer_assembly = "" # only for blocks X0
        self.overhang_start = ""
        self.overhang_stop = ""


    def get_framed_primers(self):
        """
        payload framed by the assembly primers
        only equal to the payload for all blocks but the first and last one
        """
        sequence = self.start_primer_assembly + self.payload + dfr.reverse_complement(self.stop_primer_assembly)
        return sequence


    def get_framed_sites(self):
        """
        sequence between the pool primers
        also add if exists the assembly primers (only for 1st or last)
        """
        sequence = self.get_framed_primers()
        if self.overhang_start != "":
            sequence = bsaI + self.overhang_start + sequence
        if self.overhang_stop != "":
            sequence = sequence + self.overhang_stop + dfr.reverse_complement(bsaI)

        return sequence


    def get_complete_block(self):
        # complete block sequence
        return self.start_primer_pool + self.get_framed_sites() + dfr.reverse_complement(self.stop_primer_pool)


class Protocol_molecule:

    def __init__(self, mol_id: int, intern_overhangs_list: list, payload_list: list) -> None:
        self.name = "mol_"+str(mol_id+1)
        self.mol_id = mol_id
        self.blocks_list = []
        for num, payload in enumerate(payload_list):
            block = oligo_block(num, mol_id, payload)
            self.blocks_list.append(block)

        self.assign_overhangs(intern_overhangs_list)



    def assign_overhangs(self, intern_overhangs_list):
        """
        assign a couple of overhangs for each blocks
        """
        self.blocks_list[0].overhang_stop = intern_overhangs_list[0]
        self.blocks_list[9].overhang_start = intern_overhangs_list[8]

        for i in range(1, 9):
            self.blocks_list[i].overhang_start = intern_overhangs_list[i - 1]
            self.blocks_list[i].overhang_stop = intern_overhangs_list[i]



    def assign_assembly_primers(self, Fw_primer, Rv_primer):
        """
        assign defined extern primers to the assembled molecules
        start primer for first block and stop primer to last block
        """
        self.blocks_list[0].start_primer_assembly = Fw_primer
        self.blocks_list[-1].stop_primer_assembly = Rv_primer


    def assign_pool_primers(self, Fw_primer, Rv_primer):
        """
        assign the primers used to amplify the pool
        """
        for block in self.blocks_list:
            block.start_primer_pool = Fw_primer
            block.stop_primer_pool = Rv_primer


    def get_sequences_with_stop_overhang(self):
        """
        get list of payload + stop overhang
        """
        overhang_sequences_list = []
        for block in self.blocks_list:
            seq_with_stop_overhang = block.payload + block.overhang_stop
            overhang_sequences_list.append(seq_with_stop_overhang)
        return overhang_sequences_list


    def get_blocks_with_sites(self) -> list:
        """
        get list of blocks with assembly primers and full restriction sites
        """
        sites_sequences_list = []
        for block in self.blocks_list:
            sites_sequences_list.append(block.get_framed_sites())
        return sites_sequences_list


    def save_blocks(self):
        """
        save all construction steps in fasta files
        """
        if not os.path.exists("protocol_semiordered/molecules/"+self.name):
            os.makedirs("protocol_semiordered/molecules/"+self.name)

        blocks_payload_dict = {}
        blocks_assembly_primers_dict = {}
        blocks_ov_dict = {}
        blocks_with_pool_primers_dict = {}
        
        letters_names = ["A","B","C","D","E","F","G","H","I","J"]

        for i in range(10):
            block_name = "Oligo_"+ letters_names[i] + str(self.mol_id +1)

            block = self.blocks_list[i]
            blocks_payload_dict[block_name] = block.payload
            blocks_assembly_primers_dict[block_name] = block.get_framed_primers()
            blocks_ov_dict[block_name] = block.get_framed_sites()
            blocks_with_pool_primers_dict[block_name] = block.get_complete_block()


        dfr.save_dict_to_fasta(blocks_payload_dict, "protocol_semiordered/molecules/"+self.name+"/0_payload.fasta")
        dfr.save_dict_to_fasta(blocks_assembly_primers_dict, "protocol_semiordered/molecules/"+self.name+"/1_assembly_primers.fasta")
        dfr.save_dict_to_fasta(blocks_ov_dict, "protocol_semiordered/molecules/"+self.name+"/2_overhangs.fasta")
        dfr.save_dict_to_fasta(blocks_with_pool_primers_dict, "protocol_semiordered/molecules/"+self.name+"/3_pool_primers.fasta")


def cut_payload(payload_sequence):

    if len(payload_sequence) != 16420:
        print("payload bad size",len(payload_sequence))
        exit(1)

    sequences_list = []
    index = 0
    extremity_length = 157
    intern_length = 166

    for _ in range(10):
        start_block = payload_sequence[index:index+extremity_length]
        sequences_list.append(start_block)
        index += extremity_length
        for _ in range(8):
            middle_block = payload_sequence[index:index+intern_length]
            sequences_list.append(middle_block)
            index += intern_length
        end_block = payload_sequence[index:index+extremity_length]
        sequences_list.append(end_block)
        index += extremity_length

    if index != len(payload_sequence):
        print("bad end index",index,len(payload_sequence))
        exit(1)

    return sequences_list


def get_compatible_assembly_primers(protocol_molecule_list, concatenations_path) -> dict:
    """
    get a pair of primers compatible with the assembly of any blocks within
    """
    list_of_oligos = [] # list of molecules oligos with the stop overhang

    for molecule in protocol_molecule_list:
        list_of_oligos.append(molecule.get_sequences_with_stop_overhang())

    dict_of_possible_concatenations = {}
    for mol_id in range(10):
        for block_num in range(9):
            first_concat_part = list_of_oligos[mol_id][block_num]
            for other_mol_id in range(10):
                second_concat_part = list_of_oligos[other_mol_id][block_num+1]
                dict_of_possible_concatenations[str(mol_id)+"_"+str(other_mol_id)+"_"+str(block_num)] = first_concat_part + second_concat_part


    dfr.save_dict_to_fasta(dict_of_possible_concatenations, concatenations_path)

    # generate 1000 primers compatible with the defined parameters in the generator script
    pg.generate_compatible_primers(1000)

    pg.filter_primers_with_oligos(concatenations_path)

    best_pair_dict = pg.get_best_compatible_pair()

    return best_pair_dict


def get_compatible_pool_primers(protocol_molecule_list, mol_with_sites_path, assembly_primers_couple) -> dict:
    """
    get a pair of primers compatible for the amplification of every blocks
    """
    mol_with_sites_dict = {}

    for molecule in protocol_molecule_list:
        blocks_list = molecule.get_blocks_with_sites()
        for i, block in enumerate(blocks_list):
            mol_with_sites_dict[molecule.name+"_"+str(i)] = block

    dfr.save_dict_to_fasta(mol_with_sites_dict, mol_with_sites_path)

    # generate 1000 primers compatible with the defined parameters in the generator script
    pg.generate_compatible_primers(1000)

    pg.filter_primers_with_oligos(mol_with_sites_path)

    best_pair_dict = pg.get_best_compatible_pair(other_primers_dict = assembly_primers_couple)

    return best_pair_dict



def generate_protocole_molecules() -> list:
    
    overhangs_list = list(dfr.read_fasta("protocol_semiordered/overhangs_9.fasta").values())

    if not os.path.exists("protocol_semiordered/molecules"):
        os.makedirs("protocol_semiordered/molecules")

    binary_doc = ftd.convert_file_to_bits("protocol_semiordered/readme_WWW.md")
    binary_doc = binary_doc[:27366] # cut the doc to fit in the 100 blocks

    filt_binary_doc = ftd.apply_binary_filter(binary_doc)
    sequence = ftd.bdc.binary_to_dna_baa(filt_binary_doc)

    dfr.save_sequence_to_fasta("protocole_payload", sequence, "protocol_semiordered/payload.fasta")

    payload_sequences_list = cut_payload(sequence)

    n_complete_mol = 10
    protocol_molecule_list = []

    for i in range(n_complete_mol):
        mol_i = Protocol_molecule(i, overhangs_list, payload_sequences_list[i*10:(i+1)*10])
        protocol_molecule_list.append(mol_i)

    # compute the assembly primers
    possible_concat_path = "protocol_semiordered/molecules/concats.fasta"
    assembly_primers_couple = get_compatible_assembly_primers(protocol_molecule_list, possible_concat_path)

    assembly_primers_sequences = list(assembly_primers_couple.values())

    for molecule in protocol_molecule_list:
        molecule.assign_assembly_primers(assembly_primers_sequences[0], assembly_primers_sequences[1])
    dfr.save_dict_to_fasta(assembly_primers_couple, "protocol_semiordered/assembly_primers.fasta")

    # compute the blocks primers
    mol_with_sites_path = "protocol_semiordered/molecules/restr_sites.fasta"
    block_primers_couple = get_compatible_pool_primers(protocol_molecule_list, mol_with_sites_path, assembly_primers_couple)
    print(block_primers_couple)
    block_primers_sequences = list(block_primers_couple.values())

    # assign the block primers and save all the construction steps
    for molecule in protocol_molecule_list:
        molecule.assign_pool_primers(block_primers_sequences[0], block_primers_sequences[1])
        molecule.save_blocks()

    dfr.save_dict_to_fasta(block_primers_couple, "protocol_semiordered/pool_primers.fasta")

    # check and put all final molecules in 1 file
    final_check()



def final_check():

    sum_of_blocks_dict = {}

    for num in range(10):
        block_dict_i = dfr.read_fasta("protocol_semiordered/molecules/mol_"+str(num+1)+"/3_pool_primers.fasta")

        for block_id, (block_name, sequence) in enumerate(block_dict_i.items()):
            n_bsaI = sc.get_forbidden_sequences_nbr(sequence)
            # block 0 & 9 have 1 bsaI, others have 2
            if (n_bsaI > 2 and block_id != 0 and block_id != 9) or (n_bsaI > 1 and (block_id == 0 or block_id == 9)):
                print("bsaI found !")
                print(block_name)
                print(sequence)
                exit(0)
            if 4*"A" in sequence or 4*"T" in sequence or 4*"C" in sequence or 4*"G" in sequence:
                continue
                print("homopolymere found !")
                print(block_name)
                print(sequence[22:-22])

        sum_of_blocks_dict.update(block_dict_i) # concat the dicts

    dfr.save_dict_to_fasta(sum_of_blocks_dict, "protocol_semiordered/molecules/all_oligos.fasta")

    #template_for_idt_test(sum_of_blocks_dict, "protocol_semiordered/molecules/all_blocks_idt.xlsx")



def mini_protocole():
    blocks_path = "bigBlocks_june/5_fragments_overhang_primers_Moverhang_bsa1.fasta"
    blocks_dict = dfr.read_fasta(blocks_path)

    cut_dict = {}

    for name, seq in blocks_dict.items():
        h = len(seq)//2
        seq2 = seq[:h-20] + seq[h+20:]
        cut_dict[name] = seq2

    dfr.save_dict_to_fasta(cut_dict, "bigBlocks_june/blocks_mol1_cut.fasta")

    # generate 1000 primers compatible with the defined parameters in the generator script
    pg.generate_compatible_primers(1000)

    pg.filter_primers_with_oligos("bigBlocks_june/blocks_mol1_cut.fasta")

    best_pair_dict = pg.get_best_compatible_pair()
    [start_P, stop_P] = list(best_pair_dict.values())

    primer_dict = {}

    for name, seq in cut_dict.items():
        primer_dict[name+"_P"] = start_P + seq + dfr.reverse_complement(stop_P)

    blocks_buffer_dict = eBlocks_design.add_buffer(primer_dict)

    dfr.save_dict_to_fasta(blocks_buffer_dict, "bigBlocks_june/blocks_mol1_with_P.fasta")
    dfr.save_dict_to_fasta(best_pair_dict, "bigBlocks_june/primers.fasta")


if __name__ == "__main__":
    
    print("protocol tests...")
    
    protocol_molecule_list = generate_protocole_molecules()
                
    print("\tcompleted !")

