#!/usr/bin/python3

import os
import sys
import inspect
import argparse
import xlsxwriter

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
import eBlocks_design
import primers_generation as pg
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import sequence_control as sc


# biological constraints for block assembly
buffer_size = 15
primer_size = 20
overhang_size = 4
bsaI_size = 7


class Protocol_molecule:
    
    
    def __init__(self, name: str, n_block: int, block_size: int, payload_path: str) -> None:
        self.name = name
        self.n_block = n_block
        self.block_size = block_size
        self.intern_overhangs_list = []
        self.extern_overhangs_list = []
        self.start_primer = ""
        self.stop_primer = ""     
        self.blocks_dict = {}
        self.init_blocks(payload_path)
                
    
    def get_filename_pattern(self):
        return str(self.n_block)+"x"+str(self.block_size)+".fasta"
        
        
    def init_blocks(self, payload_path: str) -> dict:
        
        init_blocks_dict = dfr.read_fasta(payload_path)
        
        name_pattern = self.name + "_eBlock_"
        
        for num, value in init_blocks_dict.items():
            if int(num) < 10:
                block_name = self.name + "_eBlock_0"+num
            else:
                block_name = self.name + "_eBlock_"+num
            self.blocks_dict[block_name] = value
    
        self.save_blocks("0_base_payload")
        
    
    def set_overhangs(self, intern_overhangs_list, extern_overhangs_list):
        self.intern_overhangs_list = intern_overhangs_list
        self.extern_overhangs_list = extern_overhangs_list
        
        
    def save_blocks(self, file_path):
        if not os.path.exists("protocol_tests/molecules/"+self.name):
            os.makedirs("protocol_tests/molecules/"+self.name)
        dfr.save_dict_to_fasta(self.blocks_dict, "protocol_tests/molecules/"+self.name+"/"+file_path+".fasta")
        
        
    def insert_filling_payload(self, filling_payload_path):
        """
        add a payload inside the fragments, surrounded by the base payload
        """
        _, payload_sequence = dfr.read_single_sequence_fasta(filling_payload_path)
        
        len_per_block = len(payload_sequence)//self.n_block
        
        # split the blocks payload in half, and insert a part of the filling payload between
        for i, (block_name, block_sequence) in enumerate(self.blocks_dict.items()):
            half_len_block = len(block_sequence)//2
            self.blocks_dict[block_name] = block_sequence[:half_len_block] + payload_sequence[i*len_per_block:(i+1)*len_per_block] + block_sequence[half_len_block:]

        self.save_blocks("1_filler_payload")
            
    
    
    def add_intern_overhangs(self):
        """
        add the internal overhangs to the blocks
        """
        self.blocks_dict, blocks_assembly_sequence = eBlocks_design.add_intern_overhangs(self.blocks_dict, self.intern_overhangs_list)

        self.save_blocks("2_intern_overhangs")
        
        return blocks_assembly_sequence
    
    
    def add_primers(self, start_primer, stop_primer):
        
        self.start_primer = start_primer
        self.stop_primer = stop_primer
        
        self.blocks_dict = eBlocks_design.add_primers(self.blocks_dict, start_primer, stop_primer)

        self.save_blocks("3_primers")
        
    
    def add_extern_overhangs(self):
        
        self.blocks_dict = eBlocks_design.add_extern_overhangs(self.blocks_dict, self.extern_overhangs_list)
        
        self.save_blocks("4_extern_overhangs")
        
        
    def add_bsaI(self):
        
        self.blocks_dict = eBlocks_design.add_bsaI(self.blocks_dict)
        
        self.save_blocks("5_bsaI")
        
        
    def add_buffer(self):
        
        self.blocks_dict = eBlocks_design.add_buffer(self.blocks_dict)
        
        self.save_blocks("6_buffer")
        
        
def special_truncation_mol2(stop_primer, stop_extern_overhang):
    """
    exp1 : special truncation for mol 2,
    generate blocks to end the mol at a lower assembly
    """
    
    truncated_blocks_dict = {}
    mol2_path = "protocol_tests/molecules/molecule_2"
    
    if not os.path.exists(mol2_path+"_T"):
            os.makedirs(mol2_path+"_T")
    
    mol2_intern_overhangs_dict = dfr.read_fasta(mol2_path+"/2_intern_overhangs.fasta")
    
    for i in range(2, 10):
        if i < 10:
            block_name = "molecule_2_eBlock_0"+str(i)
        else:
            block_name = "molecule_2_eBlock_"+str(i)
        block_i = mol2_intern_overhangs_dict[block_name]
        # remove the intern overhang at the end
        block_i = block_i[:-overhang_size]
        # remove some payload to make place for the primer
        block_i = block_i[:-primer_size]            
        # add the stop primer
        block_i += dfr.reverse_complement(stop_primer)
        truncated_blocks_dict[block_name+"_for_Truncation"] = block_i
        
    dfr.save_dict_to_fasta(truncated_blocks_dict, mol2_path+"_T/3_primers.fasta")
    
    # add extern overhang at the end
    for name, block in truncated_blocks_dict.items():
        truncated_blocks_dict[name] = block + stop_extern_overhang
    
    dfr.save_dict_to_fasta(truncated_blocks_dict, mol2_path+"_T/4_extern_overhangs.fasta")        
    
    # add bsaI + buffer
    truncated_blocks_dict = eBlocks_design.add_bsaI(truncated_blocks_dict)
    dfr.save_dict_to_fasta(truncated_blocks_dict, mol2_path+"_T/5_bsaI.fasta")
    
    truncated_blocks_dict = eBlocks_design.add_buffer(truncated_blocks_dict)
    dfr.save_dict_to_fasta(truncated_blocks_dict, mol2_path+"_T/6_buffer.fasta")
        
        
def special_truncation_exp2(molecule_name, stop_primer, start_primer, stop_extern_overhang, start_extern_overhang, last_stop_primer=None, last_stop_extern_overhang=None):
    """
    exp2 : special truncation for mol 1 7 8 9
    add variations of blocks 4 6 to split the mol in half
    particular case for mol 1 that also need to modify block 9
    """
    mol_path = "protocol_tests/molecules/"+molecule_name
    if not os.path.exists(mol_path+"_T"):
            os.makedirs(mol_path+"_T")
    truncated_blocks_dict = {}
    
    mol_intern_overhangs_dict = dfr.read_fasta(mol_path+"/2_intern_overhangs.fasta")

    
    block_4 = mol_intern_overhangs_dict[molecule_name+"_eBlock_04"]
    # remove the intern overhang at the end
    block_4 = block_4[:-overhang_size]
    # remove some payload to make place for the primer
    block_4 = block_4[:-primer_size]            
    # add the stop primer
    block_4 += dfr.reverse_complement(stop_primer)
    truncated_blocks_dict[molecule_name+"_eBlock_04_for_Truncation"] = block_4

    block_6 = mol_intern_overhangs_dict[molecule_name+"_eBlock_06"]
    # remove the intern overhang at the end
    block_6 = block_6[overhang_size:]
    # remove some payload to make place for the primer
    block_6 = block_6[primer_size:]            
    # add the stop primer
    block_6 = start_primer + block_6
    truncated_blocks_dict[molecule_name+"_eBlock_06_for_Truncation"] = block_6
    
    # also add a truncated block 9 for mol 1
    if molecule_name == "molecule_1":
        block_9 = mol_intern_overhangs_dict[molecule_name+"_eBlock_09"]
        # remove the intern overhang at the end
        block_9 = block_9[:-overhang_size]
        # remove some payload to make place for the primer
        block_9 = block_9[:-primer_size]            
        # add the stop primer
        block_9 += dfr.reverse_complement(last_stop_primer)
        truncated_blocks_dict[molecule_name+"_eBlock_09_for_Truncation"] = block_9
    

    dfr.save_dict_to_fasta(truncated_blocks_dict, mol_path+"_T/3_primers.fasta")

    # add extern overhangs
    truncated_blocks_dict[molecule_name+"_eBlock_04_for_Truncation"] = block_4 + stop_extern_overhang
    truncated_blocks_dict[molecule_name+"_eBlock_06_for_Truncation"] = start_extern_overhang + block_6
    if molecule_name == "molecule_1":
        truncated_blocks_dict[molecule_name+"_eBlock_09_for_Truncation"] = block_9 + last_stop_extern_overhang

    dfr.save_dict_to_fasta(truncated_blocks_dict, mol_path+"_T/4_extern_overhangs.fasta")

    # add bsaI + buffer
    truncated_blocks_dict = eBlocks_design.add_bsaI(truncated_blocks_dict)
    dfr.save_dict_to_fasta(truncated_blocks_dict, mol_path+"_T/5_bsaI.fasta")
    
    truncated_blocks_dict = eBlocks_design.add_buffer(truncated_blocks_dict)
    dfr.save_dict_to_fasta(truncated_blocks_dict, mol_path+"_T/6_buffer.fasta")


def template_for_idt_test(sum_of_blocks_dict, output_path: str):
    
    molecules_blocks_list = list(sum_of_blocks_dict.items())
    
    workbook = xlsxwriter.Workbook(output_path)
    worksheet = workbook.add_worksheet()
    
    worksheet.write(0, 0, "Well Position")
    worksheet.write(0, 1, "Name")
    worksheet.write(0, 2, "Sequence")
    #output.write("Well Position;Name;Sequence\n")
    
    mol_count = 0
    for i in ["A","B","C","D","E","F","G","H", "I", "J", "K", "L", "M", "N", "O"]:
        for j in range(1,13):
            molecule = molecules_blocks_list[mol_count]
            mol_count += 1
            #output.write(i+str(j)+";"+molecule[0]+";"+molecule[1]+"\n")
            worksheet.write(mol_count, 0, i+str(j))
            worksheet.write(mol_count, 1, molecule[0])
            worksheet.write(mol_count, 2, molecule[1])
            if mol_count >= len(molecules_blocks_list):
                workbook.close()
                return
    


def generate_protocole_molecules() -> list:
    
    overhangs_list = list(dfr.read_fasta("protocol_tests/overhangs_40.fasta").values())
    primers_dict = dfr.read_fasta("protocol_tests/primers.fasta")

    if not os.path.exists("protocol_tests/molecules"):
        os.makedirs("protocol_tests/molecules")
    
    
    #__EXP 1__#
    
    doc_mol_1 = "protocol_tests/payloads/HTTP.h_10x300.fasta" # payload for mol1 and used as filler in mol 2/3/4/5/6

    mol_1 = Protocol_molecule("molecule_1", 10, 300, doc_mol_1)   
    mol_2 = Protocol_molecule("molecule_2", 10, 500, doc_mol_1)    
    mol_3 = Protocol_molecule("molecule_3", 10, 700, doc_mol_1)    
    mol_4 = Protocol_molecule("molecule_4", 10, 1000, doc_mol_1)    
    mol_5 = Protocol_molecule("molecule_5", 10, 1200, doc_mol_1)    
    mol_6 = Protocol_molecule("molecule_6", 10, 1500, doc_mol_1)
        
    mol_2.insert_filling_payload("protocol_tests/payloads/WWW.h_1x2092.fasta")
    mol_3.insert_filling_payload("protocol_tests/payloads/HyperAccess.h_1x4092.fasta")
    mol_4.insert_filling_payload("protocol_tests/payloads/HTTP.c_1x7092.fasta")
    mol_5.insert_filling_payload("protocol_tests/payloads/HTTCP.h_1x9092.fasta")
    mol_6.insert_filling_payload("protocol_tests/payloads/HTString.c_1x12092.fasta")
    
    mol_1.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_2.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_3.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_4.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_5.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_6.set_overhangs(overhangs_list[:9], overhangs_list[9:11])


    #__EXP 2__#
    
    mol_7 = Protocol_molecule("molecule_7", 9, 300, "protocol_tests/payloads/HTFTP.h_9x300.fasta")
    mol_8 = Protocol_molecule("molecule_8", 9, 300, "protocol_tests/payloads/StyleToy.h_9x300.fasta")
    mol_9 = Protocol_molecule("molecule_9", 9, 300, "protocol_tests/payloads/Makefile__1_9x300.fasta")
    
    mol_7.set_overhangs(overhangs_list[10:18], overhangs_list[18:20])
    mol_8.set_overhangs(overhangs_list[20:28], overhangs_list[28:30])
    mol_9.set_overhangs(overhangs_list[30:38], overhangs_list[38:40])
    
    
    #__EXP 3__#
    
    mol_10 = Protocol_molecule("molecule_10", 10, 300, "protocol_tests/payloads/HTFTP.c__1_10x300.fasta")
    mol_11 = Protocol_molecule("molecule_11", 10, 300, "protocol_tests/payloads/HTFTP.c__2_10x300.fasta")
    mol_12 = Protocol_molecule("molecule_12", 10, 300, "protocol_tests/payloads/HTString.h_10x300.fasta")
    mol_13 = Protocol_molecule("molecule_13", 10, 300, "protocol_tests/payloads/HTAccess.h_10x300.fasta")
    mol_14 = Protocol_molecule("molecule_14", 10, 300, "protocol_tests/payloads/TextToy.h_10x300.fasta")
        
    mol_10.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_11.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_12.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_13.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
    mol_14.set_overhangs(overhangs_list[:9], overhangs_list[9:11])
        
    
    protocol_molecule_list = [mol_1, mol_2, mol_3, mol_4, mol_5, mol_6, mol_7, 
                              mol_8, mol_9, mol_10, mol_11, mol_12, mol_13, mol_14]

    
    total_sequence = ""
    
    for molecule in protocol_molecule_list:
        total_sequence += molecule.add_intern_overhangs()
      
    #print(len(total_sequence))
    """
    
    # generate primers compatible with the full joined sequence
    compatible_primers_list = pg.generate_compatible_primers(total_sequence)
    
    # select needed primers
    selected_primers_list = pg.select_primers(compatible_primers_list, 20)
    
    for p in selected_primers_list:
        print(p+" "+str(p.count("A")+p.count("T")))
    
    exit(0)"""
    
    # ADD THE PRIMERS
        
    
    #__EXP 1__#
    
    mol_1.add_primers(primers_dict["Primer_1A"], primers_dict["Primer_1B"])
    mol_2.add_primers(primers_dict["Primer_1A"], primers_dict["Primer_1B"])
    mol_3.add_primers(primers_dict["Primer_1A"], primers_dict["Primer_1B"])
    mol_4.add_primers(primers_dict["Primer_1A"], primers_dict["Primer_1B"])
    mol_5.add_primers(primers_dict["Primer_1A"], primers_dict["Primer_1B"])
    mol_6.add_primers(primers_dict["Primer_1A"], primers_dict["Primer_1B"])
    
    
    #__EXP 2__#
    
    mol_7.add_primers(primers_dict["Primer_7A"], primers_dict["Primer_7B"])
    mol_8.add_primers(primers_dict["Primer_8A"], primers_dict["Primer_8B"])
    mol_9.add_primers(primers_dict["Primer_9A"], primers_dict["Primer_9B"])
    
    
    #__EXP 3__#
    
    mol_10.add_primers(primers_dict["Primer_1A"], primers_dict["Primer_1C"])
    mol_11.add_primers(primers_dict["Primer_1A"], primers_dict["Primer_1D"])
    mol_12.add_primers(primers_dict["Primer_7A"], primers_dict["Primer_1B"])
    mol_13.add_primers(primers_dict["Primer_7A"], primers_dict["Primer_1C"])
    mol_14.add_primers(primers_dict["Primer_7A"], primers_dict["Primer_1D"])


    # ADD EXTERN OVERHANGS
    
    for molecule in protocol_molecule_list:
        molecule.add_extern_overhangs()


    # ADD BSAI + BUFFERS
    
    for molecule in protocol_molecule_list:
        molecule.add_bsaI()
        molecule.add_buffer()
    

    # truncated mols
    
    special_truncation_mol2(primers_dict["Primer_1B"], overhangs_list[10])
    
    special_truncation_exp2("molecule_1", primers_dict["Primer_1C"], primers_dict["Primer_1D"], overhangs_list[3], overhangs_list[4], primers_dict["Primer_1B"], overhangs_list[8])
    special_truncation_exp2("molecule_7", primers_dict["Primer_7C"], primers_dict["Primer_7D"], overhangs_list[13], overhangs_list[14])
    special_truncation_exp2("molecule_8", primers_dict["Primer_8C"], primers_dict["Primer_8D"], overhangs_list[23], overhangs_list[24])
    special_truncation_exp2("molecule_9", primers_dict["Primer_9C"], primers_dict["Primer_9D"], overhangs_list[33], overhangs_list[34])
    
    
    
    # check and put all final molecules in 1 file
    
    sum_of_blocks_dict = {}
    
    for i in ["1", "1_T", "2", "2_T"] + [str(k) for k in range(3,8)] + ["7_T", "8", "8_T", "9", "9_T"] + [str(k) for k in range(10,15)]:
        block_dict_i = dfr.read_fasta("protocol_tests/molecules/molecule_"+i+"/6_buffer.fasta")
        
        for block_name, sequence in block_dict_i.items():
            n_bsaI = sc.get_forbidden_sequences_nbr(sequence)
            if n_bsaI > 2:
                print("bsaI found !")
                print(block_name)
                print(sequence)
                exit(0)
            if 4*"A" in sequence or 4*"T" in sequence or 4*"C" in sequence or 4*"G" in sequence:
                print("homopolymere found !")
                print(block_name)
                print(sequence[22:-22])
            
            sum_of_blocks_dict.update(block_dict_i) # concat the dicts   
    
    dfr.save_dict_to_fasta(sum_of_blocks_dict, "protocol_tests/molecules/all_eBlocks.fasta")

    template_for_idt_test(sum_of_blocks_dict, "protocol_tests/molecules/all_eBlocks_idt.xlsx")

    
if __name__ == "__main__":
    
    print("protocol tests...")
    
    protocol_molecule_list = generate_protocole_molecules()
    
    
                
    print("\tcompleted !")

