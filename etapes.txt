

- partie Info : écriture -

0 : dossier projet WWW avec ~70 fichiers 

1 : zip chaque fichier
2 : convertion chaque zip en binaire
3 : si binaire trop long pour tenir dans 10kB, division en sous fichiers < 10kB
4 : ajouter checksum dans chaque binaire
5 : filtrage du binaire
6 : convertion chaque binaire en séquence ACGT
7 : découpage automatique de chaque séquence en eBlocks (le moins d'eBlocks possible de la même taille) (taille entre 300-1000 -> en comptant l'ajout de primers/overhang/bsaI/buffer ensuite)
8 : ajout overhangs
9 : iThos -> creation couple primers compatible avec l'ensemble des eBlocks+overhangs
10 : ajout des primers +(du bsaI ?)+(du buffer ?) aux eBlocks
11 : donner un fasta d'eBlocks pour chaque fichier du projet

~ partie Bio ~
...

- partie Info : lecture -

0 : 1 gros fichier de reads fastq qui contient toutes les molécules

1 : clustering
2 : consensus de chaque cluster, comme on connait les 2 primers on peut mettre dans le bon sens et bien couper aux extremités
3 : extraction de la payload de chaque consensus, en fonction de la taille du consensus on devine combien d'eBlocks ça contient et donc où sont les overhangs à retirer
4 : conversion vers le binaire
5 : filtrage inverse du binaire
6 : validation du checksum, si faux -> retourner étape 2
7 : réassembler les sous-fichiers
8 : reconvertir en zip
9 : dézip chaque fichier 

