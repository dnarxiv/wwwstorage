#!/usr/bin/python3

import os
import sys
import inspect
import argparse
import xlsxwriter

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
import eBlocks_design
import primers_generation_V2 as pg
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import sequence_control as sc
import file_to_dna as ftd


# restrition site used to digest overhangs only on one side
bsaI_fw = "GGTCTCG"
bsaI_rv = "TGAGACC"

class oligo_block:

    def __init__(self, num: int, mol_id: int, payload: str):
        self.num = num
        self.payload = payload
        self.mol_id = mol_id
        self.start_primer_block = ""
        self.stop_primer_block = ""
        self.start_primer_assembly = "" # only added for start blocks
        self.stop_primer_assembly = "" # only for stop blocks
        self.overhang_start = ""
        self.overhang_stop = ""


    def get_framed_primers(self):
        """
        payload framed by the assembly primers
        only equal to the payload for all blocks but the first and last one
        """
        sequence = self.start_primer_assembly + self.payload + dfr.reverse_complement(self.stop_primer_assembly)
        return sequence


    def get_framed_sites(self):
        """
        sequence between the pool primers
        also add if exists the assembly primers (only for 1st or last)
        """
        sequence = self.get_framed_primers()
        #if self.overhang_start != "":
        sequence = bsaI_fw + self.overhang_start + sequence
        #if self.overhang_stop != "":
        sequence = sequence + self.overhang_stop + bsaI_rv

        return sequence


    def get_complete_block(self):
        # complete block sequence
        return self.start_primer_block + self.get_framed_sites() + dfr.reverse_complement(self.stop_primer_block)


class Protocol_molecule:
    # simulate the bigBlock = assembly of multiple eBlocks

    def __init__(self, mol_id: int, intern_overhangs_list: list, payload_list: list) -> None:
        self.name = "mol_"+str(mol_id+1)
        self.mol_id = mol_id
        self.blocks_list = []
        for num, payload in enumerate(payload_list):
            block = oligo_block(num, mol_id, payload)
            self.blocks_list.append(block)

        self.assign_overhangs(intern_overhangs_list)



    def assign_overhangs(self, intern_overhangs_list):
        """
        assign a couple of overhangs for each blocks
        stop overhang for block i = start overhang for block i+1
        """
        for i in range(len(self.blocks_list)):
            self.blocks_list[i].overhang_start = intern_overhangs_list[i]
            self.blocks_list[i].overhang_stop = intern_overhangs_list[i+1]


    def get_compatible_assembly_primers(self, other_assembly_primers_dict) -> dict:
        """
        get a pair of primers compatible with the assembly
        """

        # get the pair with the closests temp, and closest temp with the other primers
        #source_primers_dict=dfr.read_fasta(filtered_primers_path),
        best_pair_dict = pg.get_best_compatible_pair(other_primers_dict=other_assembly_primers_dict)

        Fw_primer_name = self.name+"_FW_" + list(best_pair_dict.keys())[0]
        Fw_primer_sequence = list(best_pair_dict.values())[0]

        Rv_primer_name = self.name+"_RV_" + list(best_pair_dict.keys())[1]
        Rv_primer_sequence = list(best_pair_dict.values())[1]

        # assign defined extern primers to the assembled molecules
        # start primer for first block and stop primer to last block
        self.blocks_list[0].start_primer_assembly = Fw_primer_sequence
        self.blocks_list[-1].stop_primer_assembly = Rv_primer_sequence

        # return the primers to use in next primers computation and save the total in fasta
        return {Fw_primer_name: Fw_primer_sequence, Rv_primer_name: Rv_primer_sequence}


    def assign_blocks_primers(self, Fw_primer, Rv_primer):
        """
        assign the primers used to amplify the pool
        """
        for block in self.blocks_list:
            block.start_primer_block = Fw_primer
            block.stop_primer_block = Rv_primer


    def get_sequences_with_stop_overhang(self):
        """
        get list of payload + stop overhang
        """
        overhang_sequences_list = []
        for block in self.blocks_list:
            seq_with_stop_overhang = block.payload + block.overhang_stop
            overhang_sequences_list.append(seq_with_stop_overhang)
        return overhang_sequences_list


    def get_blocks_with_sites(self) -> list:
        """
        get list of blocks with assembly primers and full restriction sites
        """
        sites_sequences_list = []
        for block in self.blocks_list:
            sites_sequences_list.append(block.get_framed_sites())
        return sites_sequences_list


    def add_buffer_and_save_blocks(self):
        """
        save all construction steps in fasta files
        """
        if not os.path.exists("protocol_udhr_oct24/molecules/"+self.name):
            os.makedirs("protocol_udhr_oct24/molecules/"+self.name)

        blocks_payload_dict = {}
        blocks_assembly_primers_dict = {}
        blocks_ov_dict = {}
        blocks_with_blocks_primers_dict = {}

        for i in range(len(self.blocks_list)):
            block_name = self.name + "_eBlock_"+ str(i+1)

            block = self.blocks_list[i]
            blocks_payload_dict[block_name] = block.payload
            blocks_assembly_primers_dict[block_name] = block.get_framed_primers()
            blocks_ov_dict[block_name] = block.get_framed_sites()
            blocks_with_blocks_primers_dict[block_name] = block.get_complete_block()
        # add buffer from payload parts
        blocks_buffer_dict = eBlocks_design.add_buffer(blocks_with_blocks_primers_dict)

        dfr.save_dict_to_fasta(blocks_payload_dict, "protocol_udhr_oct24/molecules/"+self.name+"/0_payload.fasta")
        dfr.save_dict_to_fasta(blocks_assembly_primers_dict, "protocol_udhr_oct24/molecules/"+self.name+"/1_assembly_primers.fasta")
        dfr.save_dict_to_fasta(blocks_ov_dict, "protocol_udhr_oct24/molecules/"+self.name+"/2_overhangs_and_bsaI.fasta")
        dfr.save_dict_to_fasta(blocks_with_blocks_primers_dict, "protocol_udhr_oct24/molecules/"+self.name+"/3_eBlocks_primers.fasta")
        dfr.save_dict_to_fasta(blocks_buffer_dict, "protocol_udhr_oct24/molecules/"+self.name+"/4_buffer.fasta")


def cut_payload(payload_sequence):
    """
    divide the payload into parts fitting in the eBlocks
    """

    if len(payload_sequence) != 42240: # size required for 12 assembly of 8 eBlocks
        print("payload bad size",len(payload_sequence))
        exit(1)

    sequences_list = []
    index = 0
    extremity_length = 425 # 20 less since an assembly primer is added
    intern_length = 445

    for _ in range(12):
        start_block = payload_sequence[index:index+extremity_length]
        sequences_list.append(start_block)
        index += extremity_length
        for _ in range(6): # 1 start block, loop for 6 middle blocks and 1 stop block
            middle_block = payload_sequence[index:index+intern_length]
            sequences_list.append(middle_block)
            index += intern_length
        end_block = payload_sequence[index:index+extremity_length]
        sequences_list.append(end_block)
        index += extremity_length

    if index != len(payload_sequence):
        print("bad end index",index,len(payload_sequence))
        exit(1)

    return sequences_list




def get_compatible_block_primers(protocol_molecule_list, blocks_with_sites_path) -> dict:
    """
    get a pair of primers compatible for the amplification of every blocks
    """
    blocks_with_sites_dict = {}

    for molecule in protocol_molecule_list:
        blocks_list = molecule.get_blocks_with_sites()
        for i, block in enumerate(blocks_list):
            blocks_with_sites_dict[molecule.name+"_"+str(i)] = block

    dfr.save_dict_to_fasta(blocks_with_sites_dict, blocks_with_sites_path)

    # generate a lot of primers compatible with the defined parameters in the generator script
    pg.generate_compatible_primers(1000)

    # remove primers that hybridize with the oligos
    pg.filter_primers_with_oligos(blocks_with_sites_path)

    # get a pair with a close temperature
    best_pair_dict = pg.get_best_compatible_pair()

    return best_pair_dict



def generate_protocole_molecules() -> list:

    # load overhangs used
    overhangs_list = list(dfr.read_fasta("protocol_udhr_oct24/overhangs_11.fasta").values())

    # init output dir
    if not os.path.exists("protocol_udhr_oct24/molecules"):
        os.makedirs("protocol_udhr_oct24/molecules")
    if not os.path.exists("protocol_udhr_oct24/temp"):
        os.makedirs("protocol_udhr_oct24/temp")

    # extract the binary payload
    binary_doc = ftd.convert_file_to_bits("protocol_udhr_oct24/udhr_used.txt")

    # apply filter to "shuffle" the bits
    filt_binary_doc = ftd.apply_binary_filter(binary_doc)
    # convert payload bits to dna sequence
    sequence = ftd.bdc.binary_to_dna_baa(filt_binary_doc)

    dfr.save_sequence_to_fasta("protocole_payload", sequence, "protocol_udhr_oct24/payload.fasta")

    # cut the sequence into blocks parts
    payload_sequences_list = cut_payload(sequence)

    n_complete_mol = 12
    protocol_molecule_list = []

    # init bigBlocks with payload parts
    for i in range(n_complete_mol):
        mol_i = Protocol_molecule(i, overhangs_list, payload_sequences_list[i*8:(i+1)*8])
        protocol_molecule_list.append(mol_i)

    # get all bigBlocks concatenations
    bigBlock_concat_dict = {}
    for molecule in protocol_molecule_list:
        assembled_bigBlock = ""
        for block in molecule.blocks_list[:-1]:
            assembled_bigBlock += block.payload + block.overhang_stop
        assembled_bigBlock += molecule.blocks_list[-1].payload
        bigBlock_concat_dict["concat_"+molecule.name] = assembled_bigBlock

    concatenations_path = "protocol_udhr_oct24/temp/concatenations.fasta"
    dfr.save_dict_to_fasta(bigBlock_concat_dict, concatenations_path)

    # compute the assembly primers

    # generate a lot of primers compatible with the defined parameters in the generator script
    pg.generate_compatible_primers(4000)

    # remove the primers that hybridize with eBlocks
    pg.filter_primers_with_oligos(concatenations_path)

    assembly_primers_dict = {}
    # get a couple of compatible primers with close temp for each bigBlock, and compatible with other selected primers
    for molecule in protocol_molecule_list:
        # allows a hybridation of 5 with other pairs of primers
        assembly_primers_couple = molecule.get_compatible_assembly_primers(assembly_primers_dict)
        assembly_primers_dict.update(assembly_primers_couple)

    dfr.save_dict_to_fasta(assembly_primers_dict, "protocol_udhr_oct24/assembly_primers.fasta")

    # compute 2 primers used to amplify all the single blocks at the same time
    blocks_with_sites_path = "protocol_udhr_oct24/temp/all_blocks_with_restr_sites.fasta"
    block_primers_couple = get_compatible_block_primers(protocol_molecule_list, blocks_with_sites_path)

    block_primers_sequences = list(block_primers_couple.values())

    # assign the block primers and save all the construction steps
    for molecule in protocol_molecule_list:
        molecule.assign_blocks_primers(block_primers_sequences[0], block_primers_sequences[1])
        molecule.add_buffer_and_save_blocks()

    dfr.save_dict_to_fasta(block_primers_couple, "protocol_udhr_oct24/eBlocks_primers.fasta")

    # check and put all final molecules in 1 file
    final_check()



def final_check():

    sum_of_blocks_dict = {}

    for num in range(12):
        block_dict_i = dfr.read_fasta("protocol_udhr_oct24/molecules/mol_"+str(num+1)+"/4_buffer.fasta")

        for block_id, (block_name, sequence) in enumerate(block_dict_i.items()):
            n_bsaI = sc.get_forbidden_sequences_nbr(sequence)
            # each block contains only 2 bsaI
            if n_bsaI > 2:
                print("more than 2 bsaI found !")
                print(block_name)
                print(sequence)
                exit(0)
            if 4*"A" in sequence or 4*"T" in sequence or 4*"C" in sequence or 4*"G" in sequence:
                #continue
                print("homopolymere found !")
                print(block_name)
                #print(sequence[22:-22])

        sum_of_blocks_dict.update(block_dict_i) # concat the dicts

    # save all eBlocks in the same file and numbered
    all_eBlocks_list = list(sum_of_blocks_dict.items())
    all_eBlocks_dict = {}
    for mol_num in range(0,12):
        for block_num in range(0,8):
            eblock_num = mol_num*8+block_num
            eblock_name = "eBlock_{:02d}".format(eblock_num+1)+"_mol{:02d}".format(mol_num+1)+"_"+str(block_num+1)
            all_eBlocks_dict[eblock_name] = all_eBlocks_list[eblock_num][1]

    dfr.save_dict_to_fasta(all_eBlocks_dict, "protocol_udhr_oct24/all_final_eBlocks.fasta")

    template_for_idt_test(all_eBlocks_dict, "protocol_udhr_oct24/all_eBlocks_idt.xlsx")


def template_for_idt_test(sum_of_blocks_dict, output_path: str):
    """
    write the blocks in a table to test the validity on idt website
    https://eu.idtdna.com/site/order/plate/eblocks
    """
    molecules_blocks_list = list(sum_of_blocks_dict.items())

    workbook = xlsxwriter.Workbook(output_path)
    worksheet = workbook.add_worksheet()

    worksheet.write(0, 0, "Well Position")
    worksheet.write(0, 1, "Name")
    worksheet.write(0, 2, "Sequence")
    #output.write("Well Position;Name;Sequence\n")

    mol_count = 0
    for i in ["A","B","C","D","E","F","G","H", "I", "J", "K", "L", "M", "N", "O"]:
        for j in range(1,9):
            molecule = molecules_blocks_list[mol_count]
            mol_count += 1
            #output.write(i+str(j)+";"+molecule[0]+";"+molecule[1]+"\n")
            worksheet.write(mol_count, 0, i+str(j))
            worksheet.write(mol_count, 1, molecule[0])
            worksheet.write(mol_count, 2, molecule[1])
            if mol_count >= len(molecules_blocks_list):
                workbook.close()
                return



if __name__ == "__main__":
    
    print("protocol tests...")
    
    protocol_molecule_list = generate_protocole_molecules()
                
    print("\tcompleted !")

